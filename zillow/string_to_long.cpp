#include <string>
#include <iostream>
#include <cstdlib>

long stringToLong(std::string s)
{
    return static_cast<long>(atoi(s.c_str()));
}

int main()
{
    long i;
    std::cout << "Size of i (long): " << sizeof(i) << std::endl;
    std::string s;
    std::cout << "Size of s (string): " << sizeof(s) << std::endl;
    std::cout << "Max size: " << s.max_size() << std::endl;

    i = stringToLong("2147483647");

    if (i == 2147483647)
        std::cout << "Success" << std::endl;
    else
        std::cout << "Failure" << std::endl;

    std::cout << "Size of i (long): " << sizeof(i) << std::endl;

    return 0;
}