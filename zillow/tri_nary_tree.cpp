#include <iostream>
#include <stdexcept>
#include <string>
#include <stack>

using namespace std;

template <typename TYPE>
class tri_ary_tree
{
private:
    class node
    {
    public:
        node(TYPE key_value)
        {
            m_left = NULL;
            m_middle = NULL;
            m_right = NULL;
            m_key_value = key_value;
        }
        node* m_left;
        node* m_middle;
        node* m_right;
        TYPE m_key_value;
    };
    void insert(TYPE key_value, node* leaf);
    node* search(TYPE key_value, node* leaf);
    void remove(TYPE key_value, node* leaf);
    node* find_min_of_subtree(node* leaf);
    void destroy_tree(node* leaf);
    void in_order_print(node* leaf);

    node* m_root;
    int m_node_count;

public:
    tri_ary_tree();
    ~tri_ary_tree();
    void insert(TYPE key_value);
    node* search(TYPE key_value);
    void remove(TYPE key_value);
    void destroy_tree();
    void in_order_print();
    TYPE get_node_count();
};

/******************************************************************************/

template <typename TYPE>
tri_ary_tree<TYPE>::tri_ary_tree()
{
    m_root = NULL;
    m_node_count = 1;
}

template <typename TYPE>
void tri_ary_tree<TYPE>::destroy_tree(node* leaf)
{
    if (leaf)
    {
        destroy_tree(leaf->m_left);
        destroy_tree(leaf->m_middle);
        destroy_tree(leaf->m_right);
        delete leaf;
    }
}

template <typename TYPE>
void tri_ary_tree<TYPE>::destroy_tree()
{
    destroy_tree(m_root);
}

template <typename TYPE>
tri_ary_tree<TYPE>::~tri_ary_tree()
{
    destroy_tree();
}

template <typename TYPE>
void tri_ary_tree<TYPE>::insert(TYPE key_value, node* leaf)
{
    if (key_value < leaf->m_key_value)
    {
        if (leaf->m_left)
        {
            insert(key_value, leaf->m_left);
        }
        else
        {
            leaf->m_left = new node(key_value);
            m_node_count++;
        }
    }
    else if (key_value == leaf->m_key_value)
    {
        if (leaf->m_middle)
        {
            insert(key_value, leaf->m_middle);
        }
        else
        {
            leaf->m_middle = new node(key_value);
            m_node_count++;
        }
    }
    else // if (key_value > leaf->m_key_value)
    {
        if (leaf->m_right)
        {
            insert(key_value, leaf->m_right);
        }
        else
        {
            leaf->m_right = new node(key_value);
            m_node_count++;
        }
    }
}

template <typename TYPE>
typename tri_ary_tree<TYPE>::node* tri_ary_tree<TYPE>::search(TYPE key_value, node* leaf)
{
    if (leaf)
    {
        if (key_value == leaf->m_key_value)
        {
            return leaf;
        }
        if (key_value < leaf->m_key_value)
        {
            return search(key_value, leaf->m_left);
        }
        else if (key_value == leaf->m_key_value)
        {
            return search(key_value, leaf->m_middle);
        }
        else
        {
            return search(key_value, leaf->m_right);
        }
    }
    else return NULL;
}

template <typename TYPE>
typename tri_ary_tree<TYPE>::node* tri_ary_tree<TYPE>::find_min_of_subtree(node* leaf)
{
    if (leaf)
    {
        while (leaf->m_left)
        {
            return find_min_of_subtree(leaf->m_left);
        }
    }
    return leaf;
}

/*
Pseudocode for removing a node (no balancing) using a local stack

1) NTD has no children
    if root
        delete NTD
    else
        find NTD's predecessor
        set predecessor's ptr (to NTD) to NULL
        delete NTD
2) NTD has a middle child
    if NTD's middle child also has a middle child
        set NTD's middle ptr to NTD's middle's middle child
        delete NTD
    else
        delete NTD's middle child
3) NTD has a right child
    if root
        set new root to NTD's right child
        delete NTD
    else find minimum node of right subtree
        if min has a middle child
            if min's middle child also has a middle child
                set min's middle ptr to min's middle's middle child
            else
                delete min's middle child
        if min has no children
            find min's predecessor
            set predecessor's ptr (to min) to NULL
            delete min
        if min has a right child
            find min's predecessor
            set predecessor's left ptr to min's right child
            delete min
4) NTD has a left child
    if root
        set new root to NTD's left child
        delete NTD
    else
        find NTD's predecessor
        set predecessor's ptr (to NTD) to NTD's left child
        delete NTD
*/

template <typename TYPE>
void tri_ary_tree<TYPE>::remove(TYPE key_value, node* leaf)
{
    // Reference: NTD = node to delete
    stack<node*> node_stack;
    TYPE node_value = leaf->m_key_value;

    while (node_value != key_value)
    {
        node_stack.push(leaf);
        if (key_value < leaf->m_key_value && leaf->m_left)
        {
            leaf = leaf->m_left;
        }
        else if (key_value > leaf->m_key_value && leaf->m_right)
        {
            leaf = leaf->m_right;
        }
        node_value = leaf->m_key_value;
    }

    // NTD has no children
    if (!leaf->m_left && !leaf->m_middle && !leaf->m_right)
    {
        // if root
        if (node_stack.empty())
        {
            delete leaf;
            m_root = NULL;
            m_node_count = 0;
        }
        else
        {
            node* predecessor = node_stack.top();
            node_stack.pop();
            if (predecessor->m_left == leaf)
            {
                predecessor->m_left = NULL;
            }
            else if (predecessor->m_middle == leaf)
            {
                predecessor->m_middle = NULL;
            }
            else // predecessor->m_right == leaf
            {
                predecessor->m_right = NULL;
            }
            delete leaf;
            m_node_count--;
        }
    }
    // NTD has a middle child
    else if (leaf->m_middle)
    {
        node* temp = leaf->m_middle;
        if (leaf->m_middle->m_middle)
        {
            leaf->m_middle = temp->m_middle;
        }
        else
        {
            leaf->m_middle = NULL;
        }
        delete temp;
        m_node_count--;
    }
    // NTD has a right child
    else if (leaf->m_right)
    {
        // if root
        if (node_stack.empty())
        {
            node* temp = leaf;
            m_root = leaf->m_right;
            delete temp;
            m_node_count--;
        }
        else
        {
            node* min = find_min_of_subtree(leaf->m_right);
            // if min has a middle child
            if (min->m_middle)
            {
                node* temp = min->m_middle;
                if (min->m_middle->m_middle)
                {
                    min->m_middle = temp->m_middle;
                }
                else
                {
                    min->m_middle = NULL;
                }
                delete temp;
                m_node_count--;
            }
            // if min has no children or just a right child
            else if ((!min->m_left && !min->m_middle && !min->m_right)
                    || (min->m_right))
            {
                leaf->m_key_value = min->m_key_value;
                stack<node*> min_stack;
                min_stack.push(leaf->m_right);
                while (node_value != key_value)
                {
                    min_stack.push(leaf);
                    if (key_value != leaf->m_key_value && leaf->m_left)
                    {
                        leaf = leaf->m_left;
                    }
                }
                node* min_predecessor = min_stack.top();
                min_stack.pop();
                if (min->m_right)
                {
                    min_predecessor->m_left /* (min) */ = min->m_right;
                }
                else
                {
                    min_predecessor->m_left /* (min) */ = NULL;
                }
                delete min;
                m_node_count--;
            }
        }
    }
    // NTD has a left child
    else if (leaf->m_left)
    {
        // if root
        if (node_stack.empty())
        {
            m_root = leaf->m_left;
            delete leaf;
            m_node_count--;
        }
        else
        {
            node* predecessor = node_stack.top();
            node_stack.pop();
            if (predecessor->m_left == leaf)
            {
                predecessor->m_left = leaf->m_left;
            }
            else if (predecessor->m_middle == leaf)
            {
                predecessor->m_middle = leaf->m_left;
            }
            else // if predecessor->m_right == leaf
            {
                predecessor->m_right = leaf->m_left;
            }
            delete leaf;
            m_node_count--;
        }
    }
}

template <typename TYPE>
void tri_ary_tree<TYPE>::insert(TYPE key_value)
{
    if (m_root)
    {
        insert(key_value, m_root);
    }
    else
    {
        m_root = new node(key_value);
        m_node_count++;
    }
}

template <typename TYPE>
typename tri_ary_tree<TYPE>::node* tri_ary_tree<TYPE>::search(TYPE key_value)
{
    return search(key_value, m_root);
}

template <typename TYPE>
void tri_ary_tree<TYPE>::remove(TYPE key_value)
{
    if (m_root)
    {
        if (search(key_value, m_root))
        {
            remove(key_value, m_root);
        }
        else
        {
            cerr << "\nError: value not in tree\n";
        }
    }
}

template <typename TYPE>
TYPE tri_ary_tree<TYPE>::get_node_count()
{
    return m_node_count;
}

template <typename TYPE>
void tri_ary_tree<TYPE>::in_order_print(node* leaf)
{
    if (!leaf) return;
    in_order_print(leaf->m_left);
    cout << leaf->m_key_value << ' ';
    in_order_print(leaf->m_middle);
    in_order_print(leaf->m_right);
}

template <typename TYPE>
void tri_ary_tree<TYPE>::in_order_print()
{
    in_order_print(m_root);
}


/******************************************************************************/

int main()
{
    tri_ary_tree<float> tr;
    tr.insert(3.1);
    tr.insert(1.2);
    tr.insert(4.3);
    tr.insert(5.4);
    tr.insert(1.5);

    cout << "Node count: " << tr.get_node_count() << endl;

    if (tr.search(5.4)) cout << "Found!" << endl;
    else cout << "Not found!" << endl;
    if (tr.search(4.3)) cout << "Found!" << endl;
    else cout << "Not found!" << endl;
    if (tr.search(2)) cout << "Found!" << endl;
    else cout << "Not found!" << endl;
    if (tr.search(1.5)) cout << "Found!" << endl;
    else cout << "Not found!" << endl;
    if (tr.search(3.1)) cout << "Found!" << endl;
    else cout << "Not found!" << endl;
    if (tr.search(6)) cout << "Found!" << endl;
    else cout << "Not found!" << endl;

    tr.in_order_print();

    tr.remove(5.4);
    if (tr.search(5.4)) cout << "Found!" << endl;
    else cout << "Not found!" << endl;
    tr.in_order_print();
    tr.remove(4.3);
    tr.remove(100);
    if (tr.search(4.3)) cout << "Found!" << endl;
    else cout << "Not found!" << endl;
    tr.in_order_print();
    tr.remove(1.5);
    if (tr.search(1.5)) cout << "Found!" << endl;
    else cout << "Not found!" << endl;
    tr.in_order_print();
    tr.remove(1.5);
    if (tr.search(1.5)) cout << "Found!" << endl;
    else cout << "Not found!" << endl;
    tr.in_order_print();

    return 0;
}