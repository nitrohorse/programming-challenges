/*
  Asana
  =====
  Given an NxM matrix representing a garden, each entry contains a number, which is the number of carrots in that plot. A rabbit starts in the middle, or plot closest to middle with the most carrots and always goes up down left or right after eating to the plot with the most carrots. The values of the matrix will represent numbers of carrots available to the rabbit in each square of the garden. If the garden does not have an exact center, the rabbit should start in the square closest to the center with the highest carrot count.

  On a given turn, the rabbit will eat the carrots available on the square that it is on, and then move up, down, left, or right, choosing the the square that has the most carrots.If there are no carrots left on any of the adjacent squares, the rabbit will go to sleep.You may assume that the rabbit will never have to choose between two squares with the same number of carrots.

  Write a function which takes a garden matrix and returns the number of carrots the rabbit eats. You may assume the matrix is rectangular with at least 1 row and 1 column, and that it is populated with non - negative integers.

  For example,
    [[5, 7, 8, 6, 3],
     [0, 0, 7, 0, 4],
     [4, 6, 3, 4, 9],
     [3, 1, 0, 5, 8]]
  Should return 27
*/

const getValidNeighboringCells = (row, column, matrix) => {
  let validNeighboringCells = {}

  const canMoveUp = row !== 0 && matrix[row - 1][column] > 0
  const canMoveDown = row !== matrix.length - 1 && matrix[row + 1][column] > 0
  const canMoveLeft = column !== 0 && matrix[row][column - 1] > 0
  const canMoveRight = column !== matrix[0].length - 1 && matrix[row][column + 1] > 0

  if (canMoveUp) {
    validNeighboringCells['up'] = {
      'cellValue': matrix[row - 1][column],
      'row': row - 1,
      'column': column
    }
  }
  if (canMoveDown) {
    validNeighboringCells['down'] = {
      'cellValue': matrix[row + 1][column],
      'row': row + 1,
      'column': column
    }
  }
  if (canMoveLeft) {
    validNeighboringCells['left'] = {
      'cellValue': matrix[row][column - 1],
      'row': row,
      'column': column - 1
    }
  }
  if (canMoveRight) {
    validNeighboringCells['right'] = {
      'cellValue': matrix[row][column + 1],
      'row': row,
      'column': column + 1
    }
  }
  return validNeighboringCells
}

const howManyCarrotsToEat = matrix => {
  const width = matrix[0].length
  const height = matrix.length

  // TODO: determine cell close to middle of garden with max carrot count
  let row = Math.floor(width / 2)
  let column = Math.floor(height / 2)

  let carrotsToEat = matrix[row][column]

  while (true) {
    let neighboringCells = getValidNeighboringCells(row, column, matrix.slice())

    let neighboringCellWithMaxValue = {
      'cellValue': 0,
      'direction': ''
    }

    // Determine which neighboring cell has the largest amount of carrots
    for (direction in neighboringCells) {
      if (neighboringCells[direction]['cellValue'] > neighboringCellWithMaxValue['cellValue']) {
        neighboringCellWithMaxValue['cellValue'] = neighboringCells[direction]['cellValue']
        neighboringCellWithMaxValue['direction'] = direction
      }
    }

    matrix[row][column] = -1 // mark current cell as visited

    if (neighboringCellWithMaxValue['cellValue'] === 0) {
      return carrotsToEat // done
    } else {
      carrotsToEat += neighboringCellWithMaxValue['cellValue']

      // move
      row = neighboringCells[neighboringCellWithMaxValue['direction']]['row']
      column = neighboringCells[neighboringCellWithMaxValue['direction']]['column']
    }
  }
}

const matrix = [
  [5, 7, 8, 6, 3],
  [0, 0, 7, 0, 4],
  [4, 6, 3, 4, 9],
  [3, 1, 0, 5, 8]
]
console.log(matrix)

console.log(howManyCarrotsToEat(matrix))
// => 30

console.log(matrix)
// [[5, 7, 8, 6, 3],      [[-1, -1, -1, 6, 3],
//  [0, 0, 7, 0, 4], PATH  [ 0,  0, -1, 0, 4],
//  [4, 6, 3, 4, 9], ==>   [ 4,  6, -1, 4, 9],
//  [3, 1, 0, 5, 8]]       [ 3,  1,  0, 5, 8]]