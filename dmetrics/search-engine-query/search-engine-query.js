/*
  1. Search engine query

  Write a Javascript application (command-line or GUI) that prompts the user for a text string, performs a Web search (Google/Yahoo/Bing/etc - your choice) and returns the title and URL of the first result. Use any tools / libraries you wish, but you must provide instructions on how to build and run your application. Please include a brief description of your application and why you implemented it the way you did.
*/

const chalk = require('chalk')
const clear = require('clear')
const CLI = require('clui')
const figlet = require('figlet')
const inquirer = require('inquirer')
const log = console.log
const scrapeIt = require('scrape-it')
const Spinner = CLI.Spinner

clear()
log(chalk.yellow(
  figlet.textSync('First Result', {
    font: 'Small Slant',
    verticalLayout: 'default',
    horizontalLayout: 'default'
  })
))

const promptUserForQuery = async () => {
  const questions = [
    {
      name: 'query',
      type: 'input',
      message: 'What do you want to look up?',
      validate: value => {
        if (value.length) {
          return true
        } else {
          return 'Please enter your query.'
        }
      }
    }
  ]
  return inquirer.prompt(questions)
}

const getFirstSearchResult = async (query) => {
  const response = await scrapeIt(`https://www.startpage.com/do/search?q=${query}`, {
    title: '#first-result span.result_url_heading',
    url: {
      selector: 'a#title_1',
      attr: 'href'
    }
  })
  return response.data
}

const run = async () => {
  const userResponse = await promptUserForQuery()
  const query = userResponse.query

  const status = new Spinner(`Getting first result for "${query}"...`)
  status.start()

  try {
    const firstResult = await getFirstSearchResult(query)
    log(chalk
      `
      Title: {green ${firstResult.title}}
      URL: {green ${firstResult.url}}`
    )
  } catch (err) {
    console.error(err)
  } finally {
    status.stop()
    run()
  }
}

run()