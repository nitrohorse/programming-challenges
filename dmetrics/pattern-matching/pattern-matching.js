/*
  2. Pattern matching

  You are given two text files: (a) input.txt - a free-text document composed of 1 or more lines of text, and (b) patterns.txt - a set of search strings (1 per line). Write a Javascript application (command-line or GUI) that can run in three following modes:

  Required:

  Mode 1: output all the lines from input.txt that match exactly any pattern in patterns.txt

  Optional:

  Mode 2: output all the lines from input.txt that contain a match from patterns.txt somewhere in the line.

  Mode 3: output all the lines from input.txt that contain a match with edit distance <= 1 patterns.txt

  An example:
  input.txt
  Hello.  This is line 1 of text.
  and this is another.
  line 3 here
  the end

  patterns.txt
  the end
  matches
  line 3
  and this is anoother.

  Mode 1 outputs:
  the end

  Mode 2 outputs:
  line 3 here
  the end

  Mode 3 outputs:
  and this is another.
  the end
*/

const chalk = require('chalk')
const clear = require('clear')
const figlet = require('figlet')
const fs = require('fs')
const inquirer = require('inquirer')
const levenshtein = require('fast-levenshtein')
const log = console.log

clear()
log(chalk.yellow(
  figlet.textSync('Pattern Match', {
    font: 'Digital',
    verticalLayout: 'default',
    horizontalLayout: 'default'
  })
))

/*
  Mode 1
  Output all the lines from input.txt that match exactly any pattern in patterns.txt
*/
const modeOne = (input, patterns) => {
  const setOfPatterns = new Set()
  patterns.forEach(pattern => {
    if (!setOfPatterns.has(pattern)) {
      setOfPatterns.add(pattern)
    }
  })

  input.forEach(input => {
    if (setOfPatterns.has(input)) {
      log(chalk`  {green ${input}}`)
    }
  })
}

/*
  Mode 2
  Output all the lines from input.txt that contain a match from patterns.txt somewhere in the line.
*/
const modeTwo = (input, patterns) => {
  input.forEach(input => {
    patterns.forEach(pattern => {
      if (input.includes(pattern)) {
        log(chalk`  {green ${input}}`)
      }
    })
  })
}

/*
  Mode 3
  Output all the lines from input.txt that contain a match with edit distance <= 1
*/
const modeThree = (input, patterns) => {
  input.forEach(input => {
    patterns.forEach(pattern => {
      const distance = levenshtein.get(input, pattern)
      if (distance <= 1) {
        log(chalk`  {green ${input}}`)
      }
    })
  })
}

const promptUserForMode = async () => {
  const questions = [
    {
      name: 'mode',
      type: 'input',
      message: 'What is your preferred mode?',
      validate: value => {
        if (!value.length || (value > 3 || value < 1)) {
          return 'Please specify a mode between 1 and 3.'
        } else {
          return true
        }
      }
    }
  ]
  return inquirer.prompt(questions)
}

const run = async () => {
  const patternsArray = fs.readFileSync('patterns.txt').toString().split('\n')
  const inputArray = fs.readFileSync('input.txt').toString().split('\n')

  const userResponse = await promptUserForMode()
  const mode = userResponse.mode

  log(`Results for mode ${mode}: `)
  switch (mode) {
    case '1':
      modeOne(inputArray, patternsArray)
      break
    case '2':
      modeTwo(inputArray, patternsArray)
      break
    case '3':
      modeThree(inputArray, patternsArray)
      break
    default:
      break;
  }
  run()
}

run()