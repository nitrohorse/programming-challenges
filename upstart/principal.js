/*
  simulate a loan payment
  
  *imagine this on some client-facing dashboard
  *
  * principal - amount of money left on loan contract: 10000
  * outstandingInterest - amount of unpaid interest 0
  * monthlyInterestRate 0.01
  * monthlyPayment 500
  *
  * Repayment: wait a month from the last payment, over that month accrue interest.
  * accruedInterest = monthlyInterestRate * principal ( = 10000 * 0.01 = 100)
  * then make a payment. Pay off all interest before paying down principal.
  * all interest = outstandingInterest + accruedInterest
  * 500 - (all interest) = 400 >>> pays down principal
  * Summary: after one month, 10000 - 400 = 9600 of principal remains (0 outstandingInterest)
  *
  * Given the above data, and a number of months, return the principal remaining after making payments
  * (and accruing interest) for that number of months.
*/
const log = console.log

const getPrincipalAfterNMonths = (principal, numOfMonths, outstandingInterest, monthlyInterestRate, monthlyPayment) => {
  for (let index = 0; index < numOfMonths; index++) {
    const accruedInterest = monthlyInterestRate * principal
    const allInterest = outstandingInterest + accruedInterest

    const paymentBalance = monthlyPayment - allInterest

    if (paymentBalance < 0) {
      outstandingInterest = Math.abs(paymentBalance)
    } else {
      outstandingInterest = 0

      const principalMinusPaymentBalance = principal -= paymentBalance
      if (principalMinusPaymentBalance <= 0) {
        break
      }
    }
  }

  return principal
}

// log(getPrincipalAfterNMonths(10000, 1, 0, 0.01, 500))
// => 9600

// log(getPrincipalAfterNMonths(10000, 2, 0, 0.01, 500))
// => 9600-404 = 9196

log(getPrincipalAfterNMonths(10000, 7 * 12, 200, 0.01, 500))
// => 9800 => accrued = 98, allInterest = (0 + 98) = 98, paymentBalance = 9398

//accruedInterest => 98,
//allInterest => 600 + 98
//paymentBalance = 500 - 698 = -198