const log = console.log

const dfs = (grid, i, j) => {
  if (i < 0 || i >= grid.length
    || j < 0 || j >= grid[0].length
    || grid[i][j] !== '1') {
    return // out of bounds
  }
  grid[i][j] = '0' // mark as visited

  // check neighboring cells
  dfs(grid, i + 1, j)
  dfs(grid, i - 1, j)
  dfs(grid, i, j + 1)
  dfs(grid, i, j - 1)
}

const transformIntoMatrix = zombies => {
  const zombieMatrix = new Array()

  for (let i = 0; i < zombies.length; i++) {
    const zombieRow = new Array()
    for (let j = 0; j < zombies[0].length; j++) {
      zombieRow.push(zombies[i][j])
    }
    zombieMatrix.push(zombieRow)
  }
  return zombieMatrix
}

const zombieCluster = zombies => {
  zombies = transformIntoMatrix(zombies)
  console.log(zombies)

  let count = 0

  for (let i = 0; i < zombies.length; i++) {
    for (let j = 0; j < zombies[0].length; j++) {
      if (zombies[i][j] === '1') {
        dfs(zombies, i, j)
        count++
      }
    }
  }
  return count
}

const grid1 = [
  [1, 1, 0, 0, 0],
  [1, 1, 0, 0, 0],
  [0, 0, 1, 0, 0],
  [0, 0, 0, 1, 1],
]

const grid2 = [
  [1, 1, 1, 1, 0],
  [1, 1, 0, 1, 0],
  [1, 1, 0, 0, 0],
  [0, 0, 0, 0, 0],
]

const grid3 = ['1100', '1110', '0110', '0001']

log(zombieCluster(grid3))