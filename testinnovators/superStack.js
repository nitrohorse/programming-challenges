class SuperStack {
  constructor() {
    this.superStack = new Array()
  }

  push(element) {
    this.superStack.push(element)
  }

  pop() {
    return this.superStack.pop()
  }

  // add k to each of the bottom e elements in the stack
  inc(e, k) {
    for (let i = 0; i < e; i++) {
      this.superStack[i] += k
    }
  }

  getTopOfStack() {
    return this.superStack[this.superStack.length - 1]
  }

  printTopOfStack() {
    const topOfStack = this.getTopOfStack()
    if (topOfStack === undefined) {
      console.log('EMPTY')
    } else {
      console.log(topOfStack)
    }
  }
}

const superStack = operations => {
  const ss = new SuperStack()

  operations.forEach(operation => {
    if (operation.startsWith('push')) {
      ss.push(Number(operation.split(' ')[1]))
    } else if (operation.startsWith('pop')) {
      ss.pop()
    } else if (operation.startsWith('inc')) {
      ss.inc(Number(operation.split(' ')[1]), Number(operation.split(' ')[2]))
    } else {
      // invalid
    }
    ss.printTopOfStack()
  })
}

const operations = [
  'push 4',
  'pop',
  'push 3'
]
superStack(operations)















