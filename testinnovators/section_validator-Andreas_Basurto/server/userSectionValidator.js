'use strict'

class UserSectionValidator {
  constructor () {
    this.errors = []
    this.decoratorsList = []
    this.decorators = {
      /*
        Anything done by an admin or a demo account should be ignored.
        These can be identified by the user field.
      */
      isUser: {
        validate: function (userSection, args) {
          if (userSection.user === 'admin' || userSection.user === 'demo') {
            this.errors.push('User is admin or demo')
          }
        }
      },
      /*
        A section that has at least 50% blank is invalid.
      */
      areAnswersFilledOut: {
        validate: function (userSection, args) {
          const half = Math.floor(userSection.answers.length / 2)
          let numberOfBlankAnswers = 0
          for (let i = 0; i < userSection.answers.length; i++) {
            const answer = userSection.answers[i]

            if (numberOfBlankAnswers >= half) {
              this.errors.push('At least 50% of the answers are blank')
              break
            }

            if (answer.blank === 1) {
              numberOfBlankAnswers++
            }
          }
        }
      },
      /*
        Only a status of completed should be considered for validation.
        Other status should be skipped. ready|inProgress|completed
      */
      isStatusCompleted: {
        validate: function (userSection, args) {
          if (userSection.status !== 'completed') {
            this.errors.push('Incomplete status')
          }
        }
      },
      /*
        If more than 50% of the questions have a duration < 2 sec.
      */
      areAnswersNotGuessed: {
        validate: function (userSection, args) {
          const half = Math.floor(userSection.answers.length / 2)

          let numberOfGuesses = 0
          for (let i = 0; i < userSection.answers.length; i++) {
            const answer = userSection.answers[i]

            if (numberOfGuesses >= half) {
              this.errors.push('More than 50% of the questions have a duration less than 2 seconds')
              break
            }

            if (answer.duration < 2) {
              numberOfGuesses++
            }
          }
        }
      },
      /*
        If a single question has a duration > 15 min.
      */
      areAnswersWithinMaxDuration: {
        validate: function (userSection, args) {
          for (let i = 0; i < userSection.answers.length; i++) {
            const answer = userSection.answers[i]

            if (answer.duration > 15) {
              this.errors.push('At least one question has a duration greater than 15 minutes')
              break
            }
          }
        }
      },
      /*
        Essays are always invalid. Identified in the sectionType
      */
      isNotEssay: {
        validate: function (userSection, args) {
          if (userSection.sectionType === 'essay') {
            this.errors.push('Section type is essay')
          }
        }
      },
      /*
        All userSections between 11-33 (or some arbitrary range) are invalid due
        to a bug in our system when those were taken.
      */
      isNotWithinRange: {
        validate: function (userSection, args) {
          const beginningId = args[0]
          const endId = args[1]

          if (userSection.userSectionId >= beginningId &&
            userSection.userSectionId <= endId) {
            this.errors.push('Within invalid user section range')
          }
        }
      }
    }
  }

  decorate (name, args) {
    this.decoratorsList.push({ name, args })
  }

  validate (userSection) {
    this.decoratorsList.forEach(decorator => {
      const name = decorator.name
      const args = decorator.args
      this.decorators[name].validate.call(this, userSection, args)
    })
  }
}

module.exports = UserSectionValidator
