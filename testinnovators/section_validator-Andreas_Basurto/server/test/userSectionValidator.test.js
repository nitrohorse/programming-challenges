'use strict'

const log = console.log
const chai = require('chai')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')
const expect = chai.expect
chai.use(sinonChai)

const UserSectionValidator = require('../userSectionValidator')

describe('UserSectionValidator', () => {
  let usValidator

  beforeEach(() => {
    usValidator = new UserSectionValidator()
  })

  describe('decorate', () => {
    it('should add the given decorator to the decorators list', () => {
      const decoratorName = 'test'
      usValidator.decorate(decoratorName)
      expect(usValidator.decoratorsList[0].name).to.equal(decoratorName)
    })
  })

  describe('validate', () => {
    it('should run each a given decorator\'s validate function', () => {
      const decoratorName = 'isUser'
      usValidator.decorate(decoratorName)
      const stub = sinon.stub(usValidator.decorators[decoratorName], 'validate')
      usValidator.validate()
      expect(stub).to.have.been.called
      stub.restore()
    })
  })

  describe('isUser', () => {
    it('should not add an error to the decorators list if the user is not \'admin\' or \'demo\'', () => {
      const decoratorName = 'isUser'
      usValidator.decorate(decoratorName)

      const userSection = { user: 'John' }
      usValidator.validate(userSection)

      expect(usValidator.errors.length).to.equal(0)
    })

    it('should add an error to the decorators list if the user is \'admin\' or \'demo\'', () => {
      const decoratorName = 'isUser'
      usValidator.decorate(decoratorName)

      const userSection = { user: 'admin' }
      usValidator.validate(userSection)

      const error = 'User is admin or demo'
      expect(usValidator.errors[0]).to.equal(error)
    })
  })
})
