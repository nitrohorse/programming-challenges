'use strict'

const express = require('express')

const mainController = require('./mainController')

const router = express.Router()
router.route('/usersection')
  .get(mainController.getAllUserSections)
router.route('/usersection/:id/analysis')
  .get(mainController.getUserSectionAnalysis)

module.exports = router
