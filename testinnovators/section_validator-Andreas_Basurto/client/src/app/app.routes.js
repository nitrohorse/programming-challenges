(() => {
  'use strict'

  angular
    .module('app')
    .config(routerConfig)

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('sections', {
        url: '/',
        component: 'sections',
      })
      .state('sectionAnalysis', {
        url: '/:id',
        component: 'sectionAnalysis'
      })

    $urlRouterProvider.otherwise('/')
  }

})()
