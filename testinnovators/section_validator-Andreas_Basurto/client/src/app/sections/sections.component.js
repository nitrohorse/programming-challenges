(() => {
  'use strict'

  angular
    .module('app')
    .component('sections', {
      controller: SectionsController,
      controllerAs: 'vm',
      templateUrl: 'app/sections/sections.view.html',
    })

  /** @ngInject */
  function SectionsController($log, $state, SectionFactory) {
    const vm = this

    vm.goToAnalysis = id => {
      $state.go('sectionAnalysis', { id })
    }

    const activate = () => {
      SectionFactory.getAll().then(response => {
        vm.sections = response.data
      })
    }

    activate()
  }

})()
