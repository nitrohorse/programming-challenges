const log = console.log

const topOfStack = stack => {
  return stack[stack.length - 1]
}

const isOpening = char => {
  return char === '(' || char === '[' || char === '{'
}

const isValid = string => {
  const validationStack = new Array()

  for (let i = 0; i < string.length; i++) {
    const char = string[i];

    if (isOpening(char)) {
      validationStack.push(char)
    } else if (topOfStack(validationStack) === '(' && char === ')'
      || topOfStack(validationStack) === '[' && char === ']'
      || topOfStack(validationStack) === '{' && char === '}') {
      validationStack.pop()
    } else {
      return false
    }
  }
  return true
}

const braces = values => {
  return values.map(string => {
    if (isValid(string)) {
      return 'YES'
    } else {
      return 'NO'
    }
  })
}

const values = ['{}[]()', '{[}]']
log(braces(values))