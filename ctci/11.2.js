const convertToArray = map => {
  let array = []
  for (key in map) {
    map[key].forEach(str => {
      array.push(str)
    })
  }
  return array
}

const sort = array => {
  let map = {}

  for (let i = 0; i < array.length; i++) {
    const str = array[i]
    const sortedStr = [...str].sort().join('')
    if (!map[sortedStr]) {
      map[sortedStr] = []
    }
    map[sortedStr].push(str)
  }
  return convertToArray(map)
}

const array = [ 'abc', 'xyz', 'cba', 'bac', 'zxy' ]
console.log(sort(array))
// => [ 'zxy', 'xyz', 'bac', 'cba', 'abc' ]