const transpose = matrix => (
  matrix[0].map((ignore, index) => (
    matrix.map(row => row[index])
  ))
)

const rotate = (matrix) => {
  return transpose(matrix.reverse())
}

const matrix = [ [1, 2, 3], [4, 5, 6], [7, 8, 9] ]
console.log(rotate(matrix))
// => [ [7, 4, 1], [8, 5, 2], [9, 6, 3]]