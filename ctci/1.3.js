const sortString = string => {
  return [...string].sort().join('')
}

const isPermutation = (string1, string2) => {
  return sortString(string1) === sortString(string2)
}

console.log(isPermutation('abcdef', 'fedacb'))
// => true

console.log(isPermutation('abcdef', 'fedacbx'))
// => false