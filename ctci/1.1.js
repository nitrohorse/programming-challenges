const uniqueChars = string => {
  let map = {}

  for (let i = string.length - 1; i >= 0; i--) {
    const char = string[i]

    if (map[char]) {
      return false
    } else {
      map[char] = true
    }
  }
  return true
}

console.log(uniqueChars('ABCDEFGHIJKL'))
// => true

console.log(uniqueChars('ABCDEFGHIJAL'))
// => false
