const compress = string => {
  let counter = 1
  let compressedString = ''
  for (let i = 0; i < string.length; i++) {
    while (string[i] === string[i + 1]) {
      counter++
      i++
    }
    compressedString += string[i] + counter
    counter = 1
  }
  return compressedString
}

console.log(compress('aabcccccaaa'))
// => a2b1c5a3