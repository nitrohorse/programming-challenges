const merge = (array1, array2, lastA, lastB) => {
  let indexOfLastA = lastA - 1
  let indexOfLastB = lastB - 1
  let indexOfMerged = lastB + lastA - 1

  while (indexOfLastA >= 0 && indexOfLastB >= 0) {
    if (array1[indexOfLastA] > array2[indexOfLastB]) {
      array1[indexOfMerged] = array1[indexOfLastA]
      indexOfLastA--
    } else {
      array1[indexOfMerged] = array2[indexOfLastB]
      indexOfLastB--
    }
    indexOfMerged--
  }

  while (indexOfLastB >= 0) {
    array1[indexOfMerged] = array2[indexOfLastB]
    indexOfMerged--
    indexOfLastB--
  }
  return array1
}

let a = [2, 3, 5, 9, false, false, false]
let b = [1, 3, 6]

console.log(merge(a, b, 4, 3))
// => [ 1, 2, 3, 3, 5, 6, 9 ]