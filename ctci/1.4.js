const replaceSpaces = string => {
  let runner = string.length - 1

  while (string[runner] === ' ') {
    runner--
  }

  let stringArray = [...string]

  for (let i = stringArray.length - 1; i >= 0; i--) {
    if (stringArray[runner] === ' ') {
      stringArray[i] = '0'
      stringArray[i - 1] = '2'
      stringArray[i - 2] = '%'
      i -= 2
    } else {
      stringArray[i] = stringArray[runner]
    }
    runner--
  }
  return stringArray.join('')
}

console.log(replaceSpaces('Mr John Smith    '))
// => 'Mr%20John%20Smith'