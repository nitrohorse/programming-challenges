const zeroRowAndColumn = matrix => {
  let row = []
  let column = []

  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[0].length; j++) {
      if (matrix[i][j] === 0) {
        row[i] = true
        column[j] = true
      }
    }
  }

  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[0].length; j++) {
      if (row[i] || column[j]) {
        matrix[i][j] = 0
      }
    }
  }
  return matrix
}

const matrix = [ [1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 0, 12] ]
console.log(zeroRowAndColumn(matrix))
// => [ [1, 2, 0, 4], [5, 6, 0, 8], [0, 0, 0, 0] ]