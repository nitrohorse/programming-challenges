#include <iostream>
#include <sstream>
#include <string>
#include <cctype>

using namespace std;

class RunLengthEncoding {
    public:
    string decode(string text);
};

string RunLengthEncoding::decode(string text) {
    // text: 0-50 chars
    stringstream decode;
    decode.str("");
    stringstream buffer;
    buffer.str("");
    int count = 0;
    
    int i = 0;
    while (i < text.length()) {
        while (isdigit(text[i])) {
            buffer << text[i];
            i++;
        }
        buffer >> count; // i.e. convert "50" to 50
        if (count > 50) {
            goto too_long;
        } else {
            int j = 0;
            while (j < count) {
                decode << text[i];
                j++;
            }
            if (decode.str().length() > 50) {
                goto too_long;
            }
        }
        i++;
    }

    cout << "final string: " << decode.str() << endl;
    return decode.str();    
    
    too_long:
        string err = "TOO LONG";
        return err;
}