#include <iostream>
#include <string>
#include <sstream>
#include <deque>

using namespace std;

class Time {
public:
    string whatTime(int seconds) {
        deque<string> s;
        stringstream ss;
        ss << seconds % 60; 
        s.push_front(ss.str());
        ss.str("");
        int minute = seconds / 60;      
        ss << minute % 60 << ":";   
        s.push_front(ss.str()); 
        ss.str("");
        int hour = minute / 60;
        ss << hour % 60 << ":";
        s.push_front(ss.str());
        ss.str("");
        while (!s.empty()) {
            ss << s.front();
            s.pop_front();
        }
        return ss.str();
    }
};