#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Soccer {
public:
    int maxPoints(vector <int> wins, vector <int> ties) {
        vector <int> teams;
        teams.resize(wins.size());
        int max_points = 0;
        
        for (int i = 0; i < teams.size(); ++i) {
            teams[i] = wins[0]*ties[i] + ties[0]*wins[i];
            if (teams[i] > max_points) {
                max_points = teams[i];
            }
        }
        
        return max_points;  
    }
};