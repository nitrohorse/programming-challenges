#include <iostream>
#include <vector>

using namespace std;

class FixTheAverage {
public:
	double add(vector <double> list, double target);
};

double FixTheAverage::add(vector <double> list, double target) {
	double sum = 0.0;
	for (int i = 0; i < list.size(); ++i) {
		sum += list[i];
	}
	return target*(list.size() + 1) - sum;
}