#include <string>
#include <algorithm>
#include <sstream>
#include <iostream>
#include <cstdlib>

using namespace std;

class SubstitutionCode 
{
    public:
    int getValue(string key, string code);
};

int SubstitutionCode::getValue(string key, string code) 
{
    stringstream valid_code;
    valid_code.str("");
    int i = 0;
    for (i = 0; i < code.length(); ++i)
    {   
        cout << "code..." <<code[i]<<endl;
        unsigned found = key.find(code[i]);
        if (found != string::npos)
        {
            valid_code << code[i];
        }       
    }
    int position = 0;
    stringstream decode;
    decode.str("");
    
    for (i = 0; i < valid_code.str().length(); ++i)
    {
        for (int j = 0; j < key.length(); ++j)
        {
            if (valid_code.str()[i] == key[j])
            {
                if (j == key.length() - 1)
                {
                    decode << 0;
                }
                else
                {
                    decode << j + 1;
                }
            } 
        }
        position = 0;
    }

    int value = 0;
    decode >> value;
    return value;
}

