#include <vector>
#include <iostream>

using namespace std;

class AimToTen {
    public:
    int need(vector <int> marks);
};

int AimToTen::need(vector <int> marks) {
    // marks includes grades received so far
    // each grade is between 0 - 10

    // Algorithm:   
    // determine average of grades so far
        // iterate through [] and calculate avg.
    // incrementally add a 10 (keep track of count) to values and calculate avg until == 9.5
    double avg = 0.0;
    double sum = 0;
    int count = 0;
    double marks_size = marks.size();
    for (int i = 0; i < marks_size; ++i) {
        sum += marks[i];
    }
    avg = sum/marks_size;
    
    while (avg < 9.5) {
        sum += 10;
        ++marks_size;
        avg = sum/marks_size;
        count++;
    }
    return count;
}