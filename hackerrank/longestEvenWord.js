/*
  Consider a string, sentence, of space-separated words where each word is a substring consisting of English alphabetic letters only. We want to find the first word in sentence having a length which is both an even number and greater than or equal to the length of any other word of even length in the sentence.
  
  For example, if sentence is Time to write great code, then the word we're looking for is Time. While code and Time are of maximal length, Time occurs first. If sentence is Write code for a great time, then the word we're looking for is code.
*/

const log = console.log

/*
  Complexity:
     Time: O(n)
     Space: O(1)
*/
function longestEvenWord(sentence) {
  const words = sentence.split(' ')

  let longestEven = ''
  let indexOfFirstEvenWord = 0
  for (let indexOfFirstEvenWord = 0; indexOfFirstEvenWord < words.length; indexOfFirstEvenWord++) {
    const word = words[indexOfFirstEvenWord]

    if (word.length % 2 === 0) {
      longestEven = word
      break
    }
  }

  if (!longestEven.length) {
    return '00'
  }

  for (let i = indexOfFirstEvenWord + 1; i < words.length; i++) {
    const word = words[i]
    if (word.length % 2 === 0 && word.length > longestEven.length) {
      longestEven = word
    }
  }

  return longestEven
}

const sentence1 = 'hey'
log(longestEvenWord(sentence1))
// => 00

const sentence2 = 'It is a pleasant day today'
log(longestEvenWord(sentence2))
// => pleasant

const sentence3 = 'You can do it the way you like'
log(longestEvenWord(sentence3))
// => like