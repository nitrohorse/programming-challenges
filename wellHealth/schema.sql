-- http://sqlfiddle.com/#!9/acb56e/1

CREATE TABLE IF NOT EXISTS `DEPARTMENT` (
  `Id` int(6) unsigned NOT NULL,
  `Name` varchar(200) NOT NULL,
  PRIMARY KEY (`Id`)
) DEFAULT CHARSET=utf8;
INSERT INTO `DEPARTMENT` (`Id`, `Name`) VALUES
  ('1', 'HR'),
  ('2', 'ENGINEERING'),
  ('3', 'SALES'),
  ('4', 'R&D');

CREATE TABLE IF NOT EXISTS `EMPLOYEE` (
  `Id` int(6) unsigned NOT NULL,
  `Name` varchar(200) NOT NULL,
  `DEPT_ID` int(6) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  FOREIGN KEY (`DEPT_ID`) REFERENCES `DEPARTMENT`(`Id`)
) DEFAULT CHARSET=utf8;
INSERT INTO `EMPLOYEE` (`Id`, `Name`, `DEPT_ID`) VALUES
  ('1', 'Jan', '3'),
  ('2', 'Bill', '1'),
  ('3', 'Val', '2'),
  ('4', 'Art', '2'),
  ('5', 'Gru', '2'),
  ('6', 'Penn', '1'),
  ('7', 'Alan', '3');