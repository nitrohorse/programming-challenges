/*
  A line has formed to buy tickets for a concert.  In order to delay a shortage caused by brokers buying large blocks of tickets, venue management has decided to sell only one ticket at a time. Buyers have to to wait through line again if they want to buy more tickets.  Jesse is standing in line and has a number of tickets to purchase.

  Given a list of ticket buyers with their numbers of desired tickets, determine how long it will take Jesse to purchase his tickets.  Jesse's position in line will be stated, and each transaction takes 1 unit of time.  For your purposes, no time is spent moving to the back of the line.

  Sample Input
  5
  2
  6
  3
  4
  5
  2

  Sample Output
  12

  Explanation

  Given tickets = [2, 6, 3, 4, 5], Jesse's position in line is marked in bold. His wait time looks like this:
    window ← 2 ← 6 ← 3 ← 4 ← 5
    window ← 6 ← 3 ← 4 ← 5 ← 1
    window ← 3 ← 4 ← 5 ← 1 ← 5
    window ← 4 ← 5 ← 1 ← 5 ← 2
    window ← 5 ← 1 ← 5 ← 2 ← 3
    window ← 1 ← 5 ← 2 ← 3 ← 4
    window ← 5 ← 2 ← 3 ← 4 (the person at the head of the line in the previous step purchased their last ticket and does not re-enter the line)
    window ← 2 ← 3 ← 4 ← 4
    window ← 3 ← 4 ← 4 ← 1
    window ← 4 ← 4 ← 1 ← 2
    window ← 4 ← 1 ← 2 ← 3
    window ← 1 ← 2 ← 3 ← 3
    window ← 2 ← 3 ← 3 (Jesse purchased his last ticket and does not re-enter the line)

  It took a total of 12 units of time to purchase 2 tickets.
*/

const log = console.log
/*
 * Complete the function below.
 */
const waitingTime = (tickets, p) => {
  // modify tickets array to mark Jessie's position for easy determination
  tickets = tickets.map(ticket => ({ ticketsToBuy: ticket }))
  tickets[p].isJessie = true

  let timer = 0

  while (true) {
    timer += 1
    if (tickets[0].isJessie && tickets[0].ticketsToBuy === 1) {
      return timer
    } else {
      // let first in line buy ticket
      tickets[0].ticketsToBuy -= 1

      const firstInLine = tickets.shift()

      if (firstInLine.ticketsToBuy !== 0) {
        // move person to back of line
        tickets.push(firstInLine)
      } else {
        // move person out of line, i.e. do nothing
      }
    }
  }
}

log(waitingTime([2, 6, 3, 4, 5], 2))