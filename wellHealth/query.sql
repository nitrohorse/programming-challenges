-- http://sqlfiddle.com/#!9/acb56e/1

select department.name, count(employee.name)
from department left join employee on department.id = employee.dept_id
group by department.name
order by count(department.name) desc, department.name asc;

-- name         count(employee.name)
-- ENGINEERING  3
-- HR           2
-- SALES        2
-- R&D          0