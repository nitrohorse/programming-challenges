/*
  Splash
  Q: Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid. The brackets must close in the correct order, "()" and "()[]{}" are all valid but "(]" and "([)]" are not.
  https://leetcode.com/problems/valid-parentheses/description/
*/

const topOfStack = stack => {
  return stack[stack.length - 1]
}

const isOpening = char => {
  return char === '(' || char === '[' || char === '{'
}

const isValid = s => {
  const validationStack = []

  for (let i = 0; i < s.length; i++) {
    const element = s[i];

    if (isOpening(element)) {
      validationStack.push(element)
    } else if (topOfStack(validationStack) === '(' && element === ')'
      || topOfStack(validationStack) === '[' && element === ']'
      || topOfStack(validationStack) === '{' && element === '}') {
      validationStack.pop()
    } else {
      return false
    }
  }
  return true
}

console.log(isValid('((()))'))
// => true

console.log(isValid(')()'))
// => false

console.log(isValid('()()()'))
// => true

console.log(isValid('([)]'))
// => false

console.log(isValid('()[]{}'))
// => true