const friendGraph = [
  {
    "name": "alice",
    "friends": [
      "bob",
      "charlie",
      "dave",
      "julia"
    ]
  },
  {
    "name": "bob",
    "friends": [
      "alice",
      "eric",
      "faye",
      "kyle"
    ]
  },
  {
    "name": "charlie",
    "friends": [
      "alice",
      "george",
      "kyle"
    ]
  },
  {
    "name": "dave",
    "friends": [
      "alice",
      "mike"
    ]
  },
  {
    "name": "eric",
    "friends": [
      "bob",
      "faye",
      "harry",
      "ike"
    ]
  },
  {
    "name": "faye",
    "friends": [
      "bob",
      "eric",
      "mike"
    ]
  },
  {
    "name": "george",
    "friends": [
      "charlie",
      "harry",
      "julia"
    ]
  },
  {
    "name": "harry",
    "friends": [
      "eric",
      "george",
      "oscar"
    ]
  },
  {
    "name": "ike",
    "friends": [
      "eric"
    ]
  },
  {
    "name": "julia",
    "friends": [
      "alice",
      "george",
      "mike"
    ]
  },
  {
    "name": "kyle",
    "friends": [
      "bob",
      "charlie",
      "lester",
      "oscar"
    ]
  },
  {
    "name": "lester",
    "friends": [
      "kyle",
      "mike",
      "oscar"
    ]
  },
  {
    "name": "malcom",
    "friends": [
      "lester"
    ]
  },
  {
    "name": "michael",
    "friends": [
      "julia",
      "lester",
      "dave"
    ]
  },
  {
    "name": "mike",
    "friends": [
      "julia",
      "lester",
      "dave"
    ]
  },
  {
    "name": "missy",
    "friends": [
      "julia",
      "lester",
      "dave"
    ]
  },
  {
    "name": "nancy",
    "friends": [
    ]
  },
  {
    "name": "oscar",
    "friends": [
      "kyle",
      "lester",
      "harry"
    ]
  }
]

//------------------------------------------------------------------------------

const log = console.log

const getFriendNameSuggestions = (prefix, friendGraph) => {
  let low = 0
  let high = friendGraph.length - 1

  while (low <= high) {
    let middle = low + Math.floor((high - low) / 2)
    const friendNameAtMiddleIndex = friendGraph[middle].name
    if (friendNameAtMiddleIndex.startsWith(prefix)) {
      const suggestions = new Array()

      // check backwards
      for (let i = middle - 1; i >= 0; i--) {
        const friendName = friendGraph[i].name

        if (friendName.startsWith(prefix)) {
          suggestions.unshift(friendName) // add to front
        } else {
          break
        }
      }

      // check forwards
      for (let i = middle; i < friendGraph.length; i++) {
        const friendName = friendGraph[i].name

        if (friendName.startsWith(prefix)) {
          suggestions.push(friendName) // add to back
        } else {
          break
        }
      }

      return suggestions
    } else if (friendNameAtMiddleIndex < prefix) {
      low = middle + 1
    } else {
      high = middle - 1
    }
  }
  return new Array()
}

log('---Testing getFriendNameSuggestions---')

log(getFriendNameSuggestions('m', friendGraph))
// => ['malcom', 'michael', 'mike', 'missy']

log(getFriendNameSuggestions('mi', friendGraph))
// => ['michael', 'mike', 'missy']

log(getFriendNameSuggestions('l', friendGraph))
// => ['lester']

//------------------------------------------------------------------------------

const getIndexOfFriendName = (targetFriendName, friendGraph) => {
  let low = 0
  let high = friendGraph.length - 1

  while (low <= high) {
    let middle = low + Math.floor((high - low) / 2)
    const friendNameAtMiddleIndex = friendGraph[middle].name
    if (friendNameAtMiddleIndex === targetFriendName) {
      return middle
    } else if (friendNameAtMiddleIndex < targetFriendName) {
      low = middle + 1
    } else {
      high = middle - 1
    }
  }
  return -1
}

const getNumOfDegrees = (firstFriendName, secondFriendName, friendGraph) => {
  const q = new Array() // queue for bfs
  const visited = {} // to mark visited friends

  const indexOfFirstFriendName = getIndexOfFriendName(firstFriendName, friendGraph)

  if (indexOfFirstFriendName === -1) {
    throw new Error(`Friend, "${firstFriendName}," does not exist in graph!`)
  }

  // store in queue a pair of the friend object and number of degrees of 
  // separation from first friend
  q.push([friendGraph[indexOfFirstFriendName], 0])

  while (q.length) {
    const pair = q.shift()
    const friend = pair[0]
    const numOfDegrees = pair[1]

    visited[friend.name] = true

    if (friend.name === secondFriendName) {
      return numOfDegrees
    }

    friend.friends.forEach(secondDegreeFriend => {
      if (!visited[secondDegreeFriend]) {
        const indexOfSecondDegreeFriend
          = getIndexOfFriendName(secondDegreeFriend, friendGraph)
        q.push([friendGraph[indexOfSecondDegreeFriend], numOfDegrees + 1])
      }
    })
  }
  return -1
}

log('---Testing getNumOfDegrees---')

log(getNumOfDegrees('alice', 'bob', friendGraph))
// => 1

log(getNumOfDegrees('alice', 'eric', friendGraph))
// => 2

log(getNumOfDegrees('faye', 'george', friendGraph))
// => 3

log(getNumOfDegrees('ike', 'oscar', friendGraph))
// => 3

log(getNumOfDegrees('fred', 'alice', friendGraph))
// => expectedly throws error
