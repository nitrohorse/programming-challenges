const friendGraph = [
  {
    "name": "alice",
    "friends": [
      "bob",
      "charlie",
      "dave",
      "julia"
    ]
  },
  {
    "name": "bob",
    "friends": [
      "alice",
      "eric",
      "faye",
      "kyle"
    ]
  },
  {
    "name": "charlie",
    "friends": [
      "alice",
      "george",
      "kyle"
    ]
  },
  {
    "name": "dave",
    "friends": [
      "alice",
      "mike"
    ]
  },
  {
    "name": "eric",
    "friends": [
      "bob",
      "faye",
      "harry",
      "ike"
    ]
  },
  {
    "name": "faye",
    "friends": [
      "bob",
      "eric",
      "mike"
    ]
  },
  {
    "name": "george",
    "friends": [
      "charlie",
      "harry",
      "julia"
    ]
  },
  {
    "name": "harry",
    "friends": [
      "eric",
      "george",
      "oscar"
    ]
  },
  {
    "name": "ike",
    "friends": [
      "eric"
    ]
  },
  {
    "name": "julia",
    "friends": [
      "alice",
      "george",
      "mike"
    ]
  },
  {
    "name": "kyle",
    "friends": [
      "bob",
      "charlie",
      "lester",
      "oscar"
    ]
  },
  {
    "name": "lester",
    "friends": [
      "kyle",
      "mike",
      "oscar"
    ]
  },
  {
    "name": "malcom",
    "friends": [
      "lester"
    ]
  },
  {
    "name": "michael",
    "friends": [
      "julia",
      "lester",
      "dave"
    ]
  },
  {
    "name": "mike",
    "friends": [
      "julia",
      "lester",
      "dave"
    ]
  },
  {
    "name": "missy",
    "friends": [
      "julia",
      "lester",
      "dave"
    ]
  },
  {
    "name": "nancy",
    "friends": [
    ]
  },
  {
    "name": "oscar",
    "friends": [
      "kyle",
      "lester",
      "harry"
    ]
  }
]

//------------------------------------------------------------------------------

const log = console.log

const getFriendNameSuggestions = (prefix, friendGraph) => {
  const index = friendGraph.findIndex(friend => friend.name.startsWith(prefix))

  const suggestions = new Array()
  
  if (index === -1) {
    return suggestions
  }

  // check forwards for additional friends
  for (let i = index; i < friendGraph.length; i++) {
    const friendName = friendGraph[i].name

    if (friendName.startsWith(prefix)) {
      suggestions.push(friendName)
    } else {
      break
    }
  }

  return suggestions
}

log('---Testing getFriendNameSuggestions---')

log(getFriendNameSuggestions('', friendGraph))
/* =>
  [ 'alice',
  'bob',
  'charlie',
  'dave',
  'eric',
  'faye',
  'george',
  'harry',
  'ike',
  'julia',
  'kyle',
  'lester',
  'malcom',
  'michael',
  'mike',
  'missy',
  'nancy',
  'oscar' ]
*/

log(getFriendNameSuggestions('m', friendGraph))
// => ['malcom', 'michael', 'mike', 'missy']

log(getFriendNameSuggestions('mi', friendGraph))
// => ['michael', 'mike', 'missy']

log(getFriendNameSuggestions('l', friendGraph))
// => ['lester']

log(getFriendNameSuggestions('z', friendGraph))
// => []

//------------------------------------------------------------------------------

const getIndexOfFriendName = (targetFriendName, friendGraph) => {
  return friendGraph.findIndex(friend => friend.name === targetFriendName)
}

const validateFriendNames = (firstFriendName, secondFriendName, friendGraph) => {
  if (!firstFriendName.length && !secondFriendName.length) {
    throw new Error(`Friends cannot be blank!`)
  }
  if (!firstFriendName.length) {
    throw new Error(`First friend cannot be blank!`)
  }
  if (!secondFriendName.length) {
    throw new Error(`Second friend cannot be blank!`)
  }
  
  const indexOfFirstFriendName = getIndexOfFriendName(firstFriendName, friendGraph)
  const indexOfSecondDegreeFriend = getIndexOfFriendName(secondFriendName, friendGraph)

  if (indexOfFirstFriendName === -1 && indexOfSecondDegreeFriend === -1) {
    throw new Error(`Friends, "${firstFriendName}" and "${secondFriendName}," do not exist in graph!`)
  }
  if (indexOfFirstFriendName === -1) {
    throw new Error(`First friend, "${firstFriendName}," does not exist in graph!`)
  }
  if (indexOfSecondDegreeFriend === -1) {
    throw new Error(`Second friend, "${secondFriendName}," does not exist in graph!`)
  }
}

const getNumOfDegrees = (firstFriendName, secondFriendName, friendGraph) => {
  validateFriendNames(firstFriendName, secondFriendName, friendGraph)
  
  const q = new Array() // queue for bfs
  const visited = {} // to mark visited friends

  const indexOfFirstFriendName = getIndexOfFriendName(firstFriendName, friendGraph)

  // store in queue a pair of the friend object and number of degrees of 
  // separation from first friend
  q.push([friendGraph[indexOfFirstFriendName], 0])

  while (q.length) {
    const pair = q.shift()
    const friend = pair[0]
    const numOfDegrees = pair[1]

    visited[friend.name] = true

    if (friend.name === secondFriendName) {
      return numOfDegrees
    }

    friend.friends.forEach(secondDegreeFriend => {
      if (!visited[secondDegreeFriend]) {
        const indexOfSecondDegreeFriend
          = getIndexOfFriendName(secondDegreeFriend, friendGraph)
        q.push([friendGraph[indexOfSecondDegreeFriend], numOfDegrees + 1])
      }
    })
  }
  return -1
}

log('---Testing getNumOfDegrees---')

log(getNumOfDegrees('alice', 'bob', friendGraph))
// => 1

log(getNumOfDegrees('alice', 'eric', friendGraph))
// => 2

log(getNumOfDegrees('faye', 'george', friendGraph))
// => 3

log(getNumOfDegrees('ike', 'oscar', friendGraph))
// => 3

// log(getNumOfDegrees('fred', 'alice', friendGraph))
// => Error: First friend, "fred," does not exist in graph!

// log(getNumOfDegrees('bob', '', friendGraph))
// => Error: Second friend cannot be blank!

// log(getNumOfDegrees('', 'bob', friendGraph))
// => Error: First friend cannot be blank!