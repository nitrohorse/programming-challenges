const log = console.log

class TreeNode {
  constructor(value, left, right) {
    this.value = value
    this.left = left
    this.right = right
  }
}

const serialize = root => {
  const nums = []

  // pre-order traversal
  const serializeHelper = root => {
    if (root) {
      nums.push(root.value)
      serializeHelper(root.left)
      serializeHelper(root.right)
    } 
  }

  serializeHelper(root)
  return nums
}

const getTopOfStack = stack => {
  return stack.length ? stack[stack.length - 1] : null
}

const deserialize = nums => {
  const stack = []
  const root = new TreeNode(nums[0], null, null)
  stack.push(root)

  for (let i = 1; i < nums.length; i++) {
    const newNode = new TreeNode(nums[i], null, null)

    if (stack.length) {
      let topOfStack = getTopOfStack(stack)

      if (newNode.value < topOfStack.value) {
        topOfStack.left = newNode
      } else {
        
        let previousTopOfStack = stack.pop()
        topOfStack = getTopOfStack(stack)

        while (stack.length && newNode.value > topOfStack.value) {
          previousTopOfStack = stack.pop()
          topOfStack = getTopOfStack(stack)
        }
        previousTopOfStack.right = newNode
      }
      stack.push(newNode)      
    }
  }
  return root
}

/*
        5
      /   \
    1       7
  /   \      \
0       3      9
      /
    2

*/
const bst = new TreeNode(5, new TreeNode(1, new TreeNode(0, null, null), new TreeNode(3, new TreeNode(2, null, null), null)), new TreeNode(7, null, new TreeNode(9, null, null)))

const nums = serialize(bst)
log('Serialized:\n', nums)
// => [ 5, 1, 0, 3, 2, 7, 9 ]

const newBST = deserialize(nums)
log('Deserialized:\n', newBST)
