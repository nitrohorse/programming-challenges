/*

    5
   /\
  2  6
 /\   \
1  4   7
  /
 3 
*/
/*
  Create an iterator class
    takes the root of a BST as arg
        - next(): return node (get next value)
          - iter.next() returns 1
          - iter.next() returns 2
        - hasNext(): boolean
          - iter.hasNext() returns true unless iter is at 7 (end of tree)
*/
/*
    Node class
      - left pointer
      - right pointer
      - value
*/

/*
  Complexity
    O(n) time
    O(n) space
    Can we improve space complexity?
*/
class Iterator {
  constructor(root) {
    this.nodes = new Array()
    inOrderTraversal(root, this.nodes)
    this.index = 0
  }

  inOrderTraversal(root, nodes) {
    if (root) {
      inOrderTraversal(root.left, nodes)
      nodes.push(root)
      inOrderTraversal(root.right, nodes)
    }
  }

  next() {
    if (this.hasNext()) {
      const node = this.nodes[this.index]
      this.index++
      return node
    } else {
      return null
    }
  }

  hasNext() {
    return this.index < this.nodes.length
  }
}

/*
  Complexity
    O(n) time
    O(n) space at worst case but on average this will only store
      in a stack the nodes between the current iterator node and
      the root.
*/
class Iterator {
  constructor(root) {
    this.stack = new Stack() // path between iterater node and root
    this.root = root

    let current = this.root
    while (current) {
      this.stack.push(current)
      current = current.left
    }
    /*
      Stack now looks like:
        1
        2
        5
    */
  }

  next() {
    const topOfStack = this.stack.pop()

    // topOfStack has no children
    if (!topOfStack.right) {
      return topOfStack
    } else {
      // topOfStack.right has right child (potential subtree)
      let current = topOfStack.right
      while (current) {
        this.stack.push(current)
        current = current.left
      }
      /*
        Stack now looks like:
          3
          4
          5
      */
      return topOfStack
    }
  }

  hasNext() {
    return !this.stack.isEmpty()
  }
}