/*
  Honor
  Q: Write a function that takes a string and a limit and returns an array where each element is made up of complete words and the total length is below or equal to the limit. Assume the limit to be greater than the longest word in the sentence.

  Example:
  s: The quick, brown fox jumped over the lazy dog.
  limit: 10
  return: [
    'The quick,',
    'brown fox',
    'jumped',
    'over the',
    'lazy dog.',
  ]
*/
const log = console.log

const stringLimiter = (s, limit) => {
  const wordsArray = s.split(' ')
  const result = new Array()
  let tempElement = wordsArray[0]

  for (let i = 0; i < wordsArray.length - 1; i++) {
    const nextWord = wordsArray[i + 1]

    if (tempElement.length + 1 + nextWord.length > limit) {
      result.push(tempElement)
      tempElement = nextWord
    } else {
      tempElement += ' ' + nextWord

      if (i === wordsArray.length - 2) {
        result.push(tempElement)
      }
    }
  }
  return result
}

/*
const stringLimiter = (s, limit) => {
  let words = s.split(' ')
  let array = []
  let temp = ''

  for (let i = 0; i < words.length; i++) {
    const word = words[i]

    if (temp.length === 0) {
      temp += word
    } else if ((temp + ' ' + word).length <= limit) {
      temp += ' ' + word
    } else {
      array.push(temp)
      temp = ''
      i--
    }
  }

  if (temp.length > 0) {
    array.push(temp)
  }
  return array
}
*/

const s = 'The quick, brown fox jumped over the lazy dog.'
const limit = 10

log(stringLimiter(s, limit))
// => [ 'The quick,', 'brown fox', 'jumped', 'over the', 'lazy dog.' ]