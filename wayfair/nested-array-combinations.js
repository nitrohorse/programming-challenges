/*
  Print out all combinations from an array of arrays.

  Example:
  Input: [['a', 'b'], ['c', 'd', 'e'], ['f', 'g']]
  Output:
  acf
  acg
  bcf
  bcg
  ...
  aeg
*/

const getCombinations = arrayOfArrays => {
  if (arrayOfArrays.length <= 1) {
    return arrayOfArrays[0]
  }

  let allArraysExceptFirstArray = arrayOfArrays.slice(1)
  let combinationsExceptFirstArray = getCombinations(allArraysExceptFirstArray)

  let firstArray = arrayOfArrays[0]
  let combinations = []

  combinationsExceptFirstArray.forEach(combinationExceptFirstArray => {
    for (let i = 0; i < firstArray.length; i++) {
      combinations.push(firstArray[i] + combinationExceptFirstArray)
    }
  })
  return combinations
}

let arrayOfArrays = [['a', 'b'], ['c', 'd', 'e'], ['f', 'g']]
console.log(getCombinations(arrayOfArrays))
/*
[ 'acf',
  'bcf',
  'adf',
  'bdf',
  'aef',
  'bef',
  'acg',
  'bcg',
  'adg',
  'bdg',
  'aeg',
  'beg' ]
*/