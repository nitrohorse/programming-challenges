/*
    https://www.interviewzen.com/question/45zh6Xm

    Implement the class "RunCounter".  It takes a single "std::string" containing only "0"s and "1"s as a parameter to its constructor.  It must implement a function member, "runs", which returns the number of runs of consecutive "0"s and "1"s of the specified length.

        ```
        #include <cassert>
        #include <string>

        class RunCounter {
        // implement this class
        };

        int main(int argc, char** argv) {
            RunCounter counter {"00110001"};
            assert(counter.runs(1) == 1);
            assert(counter.runs(2) == 2);
            assert(counter.runs(3) == 1);
            assert(counter.runs(4) == 0);
            return 0;
        }
        ```
*/

const str = "00110001"

function runs (num) {
    var map = { 1: 0, 2: 0, 3: 0, 4: 0 }
    var sum = 0
    for (var i = 0; i < str.length; i++) {
        if (str[i] == str[i+1]) {
            sum++
        } else {
            map[sum+1]++
            sum = 0
        }
    }
    return map[num]
}

console.log('Consecutive runs:', runs(1))