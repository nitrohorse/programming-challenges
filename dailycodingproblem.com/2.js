/*
  Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.

  For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].
*/
const log = console.log

const productsOfAllIntsBeforeIndex = array => {
  const productsExceptAtIndex = new Array(array.length)

  let productSoFar = 1
  for (let i = 0; i < array.length; i++) {
    productsExceptAtIndex[i] = productSoFar
    productSoFar *= array[i]
  }

  productSoFar = 1
  for (let i = array.length - 1; i >= 0; i--) {
    productsExceptAtIndex[i] *= productSoFar
    productSoFar *= array[i]
  }

  return productsExceptAtIndex
}

const array = [1, 2, 3, 4, 5]
log(productsOfAllIntsBeforeIndex(array))