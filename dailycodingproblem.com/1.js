/*
  Given a list of numbers, return whether any two sums to k.

  For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.
*/
const log = console.log

const sumToK = (array, k) => {
  const mapObj = {}

  array.forEach(element => {
    if (mapObj[element]) {
      mapObj[element]++
    } else {
      mapObj[element] = 1
    }
  })

  for (let i = 0; i < array.length; i++) {
    const element = array[i]
    if (mapObj[k - element]) {
      return true
    }
  }
  return false
}

const array = [10, 15, 3, 7]
const k = 17

log(sumToK(array, k))