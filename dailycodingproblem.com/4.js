/*
  Given an array of integers, find the first missing positive integer in linear time and constant space. In other words, find the lowest positive integer that does not exist in the array. The array can contain duplicates and negative numbers as well.

  For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.

  You can modify the input array in-place.
*/
const log = console.log

const missingInteger = array => {
  array.sort((a, b) => (a - b))

  for (let index = 0; index < array.length; index++) {
    const int = array[index]
    const nextInt = array[index + 1]
    const intPlusOne = int + 1

    if (intPlusOne !== nextInt && intPlusOne !== 0) {
      return intPlusOne
    }

  }
}

let array = [3, 4, -1, 1]
log(missingInteger(array))
// => 2

array = [1, 2, 0]
log(missingInteger(array))
// => 3