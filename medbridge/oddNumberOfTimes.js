/*
  Write a function that takes an array as a parameter. The array contains non-negative numbers.
  Every number in the array appears an even number of times, except for one number that appears an odd number of times.
  The function should return the number that appears an odd number of times.
*/
const log = console.log

/*
 * Space complexity: O(n)
 * Time complexity: O(n)
 */
const numberThatAppearsOddNumberOfTimes = array => {
  const map = new Map()

  array.forEach(element => {
    if (!map.get(element)) {
      map.set(element, 1)
    } else {
      map.delete(element)
    }
  })

  const keys = Array.from(map.keys())

  return keys.length > 0 ? keys[0] : -1
}

const array = [1, 7, 3, 7, 3, 4, 1, 7, 7]
log(numberThatAppearsOddNumberOfTimes(array))
// => 4