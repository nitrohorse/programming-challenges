const Trialspark = (() => {
  'use strict'

  let employeeThreshold = 10
  let restaurantPreferences = require('./restaurant_preferences_1k_employees.json')

  const setEmployeeThreshold = threshold => {
    if (threshold) {
      employeeThreshold = threshold
    }
  }

  const setRestaurantPreferences = inputJson => {
    if (inputJson) {
      restaurantPreferences = inputJson
    }
  }

  const setup = (threshold, inputJsonFile) => {
    setEmployeeThreshold(threshold)
    setRestaurantPreferences(require(inputJsonFile))
  }

  const pairExists = (pair, pairs) => {
    for (let i = pairs.length - 1; i >= 0; i--) {
      const localPair = pairs[i]
      if (localPair[0] === pair[0] && localPair[1] === pair[1]
        || localPair[0] === pair[1] && localPair[1] === pair[0]) {
          return true
      }
    }
    return false
  }

  const intersectionAboveThreshold = (array1, array2) => {
    let temp
    if (array2.length > array1.length) {
      temp = array2
      array2 = array1
      array1 = temp // indexOf to loop over shorter array
    }
    const intersection = array1.filter(elem => array2.indexOf(elem) > -1)
    return intersection.length >= employeeThreshold
  }

  const removeRestaurantsBelowThreshold = map => {
    for (const restaurant in map) {
      const numOfEmployees = map[restaurant].length
      if (numOfEmployees < employeeThreshold) {
        delete map[restaurant]
      }
    }
  }

  const mapRestaurantsToEmployees = map => {
    for (const employee in restaurantPreferences) {
      const employeeRestaurantPreferences = restaurantPreferences[employee]
      employeeRestaurantPreferences.forEach(restaurant => {
        if (!map[restaurant]) {
          map[restaurant] = new Array(1).fill(employee)
        } else {
          map[restaurant].push(employee)
        }
      })
    }
  }

  const findRestaurants = () => {
    let map = {}

    mapRestaurantsToEmployees(map)
    removeRestaurantsBelowThreshold(map)

    let pairs = []
    let i = 0
    for (const restaurant in map) {
      const employeeList = map[restaurant]
      let j = 0
      for (const nextRestaurant in map) {
        if (j !== i) {
          const nextEmployeeList = map[nextRestaurant]
          const pair = [restaurant, nextRestaurant]
          if (!pairExists(pair, pairs)
          && intersectionAboveThreshold(employeeList, nextEmployeeList)) {
            pairs.push(pair)
          } 
        }
        j++
      }
      i++
    }
    return pairs
  }

  const findRestaurantsAndOutputToFileAsJSON = () => {
    const pairs = findRestaurants()
    const json = JSON.stringify(pairs)
    const fs = require('fs')
    const outputFileName = 'restaurant_pairs.json'

    fs.writeFile(outputFileName, json, err => {
      if (err) {
        console.log(err)
      } else {
        console.log('JSON saved to ' + outputFileName)
      }
    })
  }

  return {
    setEmployeeThreshold,
    setRestaurantPreferences,
    setup,
    pairExists,
    intersectionAboveThreshold,
    removeRestaurantsBelowThreshold,
    mapRestaurantsToEmployees,
    findRestaurants,
    findRestaurantsAndOutputToFileAsJSON
  }
})()

module.exports = Trialspark