import argparse
import json
import random
import string

RESTAURANTS = [
    'Chop\'t',
    'Dig Inn',
    'Laut',
    'Maysville',
    'The Smith',
    'Made Nice',
    'Ilili',
    'Junoon',
    'Blossom',
    'Carmine\'s',
    'Dos Caminos',
    'Gramercy Tavern',
    'Bobo',
    'Elmo',
    'Butter',
    'Sardi\'s',
    'Hearth',
    'Becco',
    'Blue Water Grill',
    'Union Square Cafe',
    'Rosemary\'s',
    'Narcissa',
    'Esca',
    'Parker & Quinn',
    'Hill Country Chicken',
    'Hill Country Barbecue Market',
    'Mexicue',
    'Outback Steakhouse',
    'Chipotle Mexican Grill',
    'ABC Kitchen',
    'ABC Cocina',
    'Xi\'an Famous Foods'
]

EMPLOYEES = [
    'Albert Einstein',
    'Oswald Avery',
    'Elizabeth Blackwell',
    'Rachel Carson',
    'Erwin Chargaff',
    'Jacques Cousteau',
    'Francis Crick',
    'Marie Curie',
    'Charles Darwin',
    'Empedocles',
    'Ronald Fisher',
    'Rosalind Franklin',
    'Jane Goodall',
    'Daniel Bernoulli',
    'Rene Descartes',
    'Carl Friedrich Gauss',
    'Sophie Germain',
    'Grace Hopper',
    'Ada Lovelace',
    'Emmy Noether',
    'Srinivasa Ramanujan',
    'Niccolo Tartaglia',
]


def generate_restaurants(n_restuarants):
    restaurants = set()
    for _ in range(n_restuarants):
        name_length = random.randint(5, 50)
        restaurant = ''.join(random.choice(string.ascii_lowercase) for _ in range(name_length))
        restaurants.add(restaurant)
    return list(restaurants)


def generate_employee():
    name_length = random.randint(5, 25)
    first_name = ''.join(random.choice(string.ascii_lowercase) for _ in range(name_length))
    name_length = random.randint(5, 25)
    last_name = ''.join(random.choice(string.ascii_lowercase) for _ in range(name_length))
    employee = ' '.join([first_name, last_name])
    return employee


def generate_preferences(n_employees, n_restaurants, max_employee_restaurants, pretty_results):
    if pretty_results:
        restaurants = RESTAURANTS
    else:
        restaurants = generate_restaurants(n_restaurants)
    data = {}
    for i in range(n_employees):
        if pretty_results:
            employee = EMPLOYEES[i]
        else:
            employee = generate_employee()
        n_restaurants = random.randint(1, max_employee_restaurants)
        employee_restaurants = set()
        for _ in range(n_restaurants):
            random_restaurant = restaurants[random.randint(0, len(restaurants) - 1)]
            employee_restaurants.add(random_restaurant)
        data[employee] = list(employee_restaurants)
    return data


def main():
    """
    A simple CLI app for generating input datasets for the restaurant preferences problem.
    For example:
      python random_preference_generator.py -p -o 'restaurant_preferences.json'
    generates a small example dataset. See restaurant_preferences.json in this directory for the result.

    For a larger dataset, try:
      python random_preference_generator.py -n 1000 -r 100 -m 20 -o 'restaurant_preferences_1k_employees.json'
    See restaurant_preferences_1k_employees.json for the result.  
    :return: None. Writes output to a JSON file.
    """
    parser = argparse.ArgumentParser(description='Generate restaurant preference input samples.')
    parser.add_argument('-n', action='store', type=int, default=100, dest='n_employees',
                        help='Set the number of employees to generate preferences for')
    parser.add_argument('-r', action='store', type=int, default=1000, dest='n_restaurants',
                        help='Set the number of restaurants to generate preferences for')
    parser.add_argument('-m', action='store', type=int, default=20, dest='max_employee_restaurants',
                        help='Set the maximum number of restaurants an employee may prefer')
    parser.add_argument('-o', action='store', default='preferences.json', dest='outfile_name',
                        help='Set the name of the output file')
    parser.add_argument('-p', action='store_true', dest='pretty_results',
                        help='Produce results using real employee and restaurant names. '
                             'When set, -n, -r, and -m are ignored')
    args = parser.parse_args()

    if args.pretty_results:
        args.n_employees = len(EMPLOYEES)
        args.n_restaurants = len(RESTAURANTS)
        args.max_employee_restaurants = len(RESTAURANTS)

    data = generate_preferences(args.n_employees, args.n_restaurants, args.max_employee_restaurants,
                                args.pretty_results)
    with open(args.outfile_name, 'w') as outfile:
        json.dump(data, outfile)


if __name__ == '__main__':
    main()
