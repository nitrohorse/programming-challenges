# Trialspark Challenge

## Problem Statement

One of the little perks Trialspark employees enjoy is complimentary
lunch. We use this time to get to know each other better, share ideas,
and create a stronger community in which to achieve our mission. But
in this city of abundant restaurants, deciding where to eat can bring
conflict to an otherwise harmonious occasion. One way to make this
decision easier is to understand how restaurants relate to one
another. We each have a list of restaurants that we like. Can you
help us understand which restaurants relate to one another?

### Challenge

Write a program that finds all pairs of restaurants that
appear together in at least 10 different employees' lists of preferred
restaurants. You have complete freedom to choose an algorithm,
structure, and testing methodology for your program, but please
justify your choices with documentation. Please cover the following to
help us understand your solution:
1) Will your program work as our employee count grows to a thousand?
  Ten thousand? Higher?
2) What would you improve about your solution, if you had more time to
  develop it?

### Input

A JSON file containing an object representing our employees’
preferences. The object’s keys are employee names. The values are
lists of the names of restaurants they prefer.

### Output

A JSON file containing a list of objects. Each object contains
a pair of restaurants which appeared together in at least 10
employees’ preference lists.

For example, consider the following two employees’ preferences:

```json
{
  'Ronald Fisher': [
    'Bobo',
    "Chop't",
    'Elmo',
    'Blossom',
    'Narcissa',
    'Becco',
    'Parker & Quinn'
  ],
  'Jacques Cousteau': [
    'Butter',
    'Gramercy
    'Tavern',
    "Chop't",
    'Union Square Cafe',
    'Blossom',
    'Narcissa',
    'Made
    'Nice',
    'Esca'
  ]
}
```

The restaurants that both Ronald Fisher and Jacques Cousteau prefer
are Chop’t, Blossom, and Narcissa. If ten or more employees shared
these preferences, we would express this list of restaurant pairings
in our output with the following JSON:

```json
[
  ["Chop't", "Blossom"],
  ["Chop't", "Narcissa"],
  ["Blossom", "Narcissa"]
]
```

Good luck! We look forward to reviewing your solution.

## Running Solution

* Install tools:
	* [Node.js and npm](https://www.npmjs.com/get-npm)
	* [yarn](https://yarnpkg.com/en/) (optional)
* Install dependencies (Mocha & Chai): 
	* `npm install` or `yarn`
* Run tests:
  * `npm run test` or `yarn test`

`related-restaurants.js` is exported as the module, Trialspark, with all methods revealed for testing purposes.

`index.js` runs `Trialspark.findRestaurantsAndOutputToFileAsJSON()` and can be quickly run via the following `package.json` scripts:
  * `npm run start` or `yarn start` passing in `<employee threshold> <input JSON file>` as command line arguments
* Example:
  * `yarn start 10 './restaurant_preferences_1k_employees.json'`

Or run these examples straight away:
* `npm run start:example1` or `yarn start:example`
  * passes in **10** as the employee threshold and **'./restaurant_preferences_1k_employees.json'** as the input JSON file
* `npm run start:example2` or `yarn start:example2`
  * passes in **7** as the employee threshold and **'./restaurant_preferences.json'** as the input JSON file

## Algorithm
* **For simplicity assume threshold is set to 2 rather than 10**
* Input JSON file:

```json
{
  "John Smith": [
    "Chop't", "Strings", "In-N-Out", "Flacos Tacos", "Panera Bread", "Crazy 8's"
  ],
  "Frank Perkins": [
    "Chili's", "Chop't", "In-N-Out", "Burger King", "Five Guys"
  ],
  "Bill Greggi": [
    "Burger King", "Wendy's", "Chili's", "Starbucks", "Pizza Hut"
  ],
  "Carl Pilkington": [
    "Pizza Hut", "Blaze", "Flacos Tacos", "Strings", "Round Table Pizza"
  ],
  "Lou Richards": [
    "Chester's", "Chop't", "Crazy 8's", "Chipotle", "Panera Bread"
  ]
}
```

* Create a map of restaurants to employees

```javascript
{
  'Chop\'t': [ 'John Smith', 'Frank Perkins', 'Lou Richards' ],
  Strings: [ 'John Smith', 'Carl Pilkington' ],
  'In-N-Out': [ 'John Smith', 'Frank Perkins' ],
  'Flacos Tacos': [ 'John Smith', 'Carl Pilkington' ],
  'Panera Bread': [ 'John Smith', 'Lou Richards' ],
  'Crazy 8\'s': [ 'John Smith', 'Lou Richards' ],
  'Chili\'s': [ 'Frank Perkins', 'Bill Greggi' ],
  'Burger King': [ 'Frank Perkins', 'Bill Greggi' ],
  'Five Guys': [ 'Frank Perkins' ],
  'Wendy\'s': [ 'Bill Greggi' ],
  Starbucks: [ 'Bill Greggi' ],
  'Pizza Hut': [ 'Bill Greggi', 'Carl Pilkington' ],
  Blaze: [ 'Carl Pilkington' ],
  'Round Table Pizza': [ 'Carl Pilkington' ],
  'Chester\'s': [ 'Lou Richards' ],
  Chipotle: [ 'Lou Richards' ]
}
```
* Remove restaurant entries in the map if the employee list length is below the threshold

```javascript
{
  'Chop\'t': [ 'John Smith', 'Frank Perkins', 'Lou Richards' ],
  Strings: [ 'John Smith', 'Carl Pilkington' ],
  'In-N-Out': [ 'John Smith', 'Frank Perkins' ],
  'Flacos Tacos': [ 'John Smith', 'Carl Pilkington' ],
  'Panera Bread': [ 'John Smith', 'Lou Richards' ],
  'Crazy 8\'s': [ 'John Smith', 'Lou Richards' ],
  'Chili\'s': [ 'Frank Perkins', 'Bill Greggi' ],
  'Burger King': [ 'Frank Perkins', 'Bill Greggi' ],
  'Pizza Hut': [ 'Bill Greggi', 'Carl Pilkington' ]
}
```

* Iterate through the restaurants in the map and iterate against all other restaurants
  * If the pair `[ restaurant["i"], restaurant["j"] ]` is _not_ already accounted for **AND** if the number of employees in the intersection between both restaurants' employee lists is above the threshold
      *  Store the pair

* Output pairs into file as JSON (`restaurant_pairs.json`)

```json
[
  [
    "Chop't",
    "In-N-Out"
  ],
  [
    "Chop't",
    "Panera Bread"
  ],
  [
    "Chop't",
    "Crazy 8's"
  ],
  [
    "Strings",
    "Flacos Tacos"
  ],
  [
    "Panera Bread",
    "Crazy 8's"
  ],
  [
    "Chili's",
    "Burger King"
  ]
]
```