'use strict'

const assert = require('assert')
const expect = require('chai').expect
const Trialspark = require('../related-restaurants')

describe ('Trailspark', () => {

  describe ('pairExists', () => {
    let pairs = [['a', 'b'], ['c', 'd']]

    it ('should return true if pair exists', () => {
      const pair = ['a', 'b']
      const exists = Trialspark.pairExists(pair, pairs)
      expect(exists).to.equal(true)
    })

    it ('should return false if pair doesn\'t exist', () => {
      const pair = ['x', 'b']
      const exists = Trialspark.pairExists(pair, pairs)
      expect(exists).to.equal(false)
    })
  })

  describe ('intersectionAboveThreshold', () => {
    it ('should return true if the intersection between two arrays is >= to the threshold', () => {
      const array1 = [
        'Marie Curie',
        'Charles Darwin',
        'Rachel Carson',
        'Sophie Germain',
        'Grace Hopper',
        'Oswald Avery',
        'Ada Lovelace',
        'Jane Goodall',
        'Rosalind Franklin',
        'Niccolo Tartaglia',
        'Erwin Chargaff',
        'Elizabeth Blackwell',
        'Daniel Bernoulli',
        'Albert Einstein'
      ]
      const array2 = [
        'Marie Curie',
        'Carl Friedrich Gauss',
        'Grace Hopper',
        'Jane Goodall',
        'Rosalind Franklin',
        'Erwin Chargaff'
      ]
      Trialspark.setEmployeeThreshold(5)
      const aboveThreshold = Trialspark.intersectionAboveThreshold(array1, array2)
      expect(aboveThreshold).to.equal(true)
    })

    it ('should return false if the intersection between two arrays is < the threshold', () => {
      const array1 = [
        'Marie Curie',
        'Charles Darwin',
        'Rachel Carson',
        'Sophie Germain',
        'Grace Hopper',
        'Oswald Avery',
        'Ada Lovelace',
        'Jane Goodall',
        'Rosalind Franklin',
        'Niccolo Tartaglia',
        'Erwin Chargaff',
        'Elizabeth Blackwell',
        'Daniel Bernoulli',
        'Albert Einstein'
      ]
      const array2 = [
        'Marie Curie',
        'Carl Friedrich Gauss',
        'Grace Hopper',
        'Jane Goodall',
        'Rosalind Franklin',
        'Erwin Chargaff'
      ]
      Trialspark.setEmployeeThreshold(6)
      const aboveThreshold = Trialspark.intersectionAboveThreshold(array1, array2)
      expect(aboveThreshold).to.equal(false)
    })
  })

  describe ('removeRestaurantsBelowThreshold', () => {
    it ('should remove restaurant keys and corresponding employees from map if number of employees is below threshold', () => {
      const map = {
        'Restaurant 1': ['Employee 1', 'Employee 2', 'Employee 3'],
        'Restaurant 2': ['Employee 2'],
        'Restaurant 3': ['Employee 3', 'Employee 1']
      }
      Trialspark.setEmployeeThreshold(2)
      Trialspark.removeRestaurantsBelowThreshold(map)
      expect(map['Restaurant 2']).to.equal(undefined)
    })
  })

  describe ('mapRestaurantsToEmployees', () => {
    it ('should map restaurants to an employee', () => {
      const restaurantPreferences = {
        "John Smith": [
          "Chop't", "Strings", "In-N-Out"
        ]
      }
      let map = {}
      const mapResult = {
        'Chop\'t': ['John Smith'],
        'Strings': ['John Smith'],
        'In-N-Out': ['John Smith']
      }
      Trialspark.setEmployeeThreshold(2)
      Trialspark.setRestaurantPreferences(restaurantPreferences)
      Trialspark.mapRestaurantsToEmployees(map)
      const equals = JSON.stringify(map) === JSON.stringify(mapResult)
      expect(equals).to.equal(true)
    })

    it ('should map restaurants to employees', () => {
      const restaurantPreferences = {
        "John Smith": [
          "Chop't", "Strings", "In-N-Out", "Wendy's"
        ],
        "Frank Perkins": [
          "Chili's", "In-N-Out"
        ],
        "Bill Greggi": [
          "In-N-Out", "Wendy's", "Chili's", "Starbucks"
        ]
      }
      let map = {}
      const mapResult = {
        'Chop\'t': ['John Smith'],
        'Strings': ['John Smith'],
        'In-N-Out': ['John Smith', 'Frank Perkins', 'Bill Greggi'],
        'Wendy\'s': ['John Smith', 'Bill Greggi'],
        'Chili\'s': ['Frank Perkins', 'Bill Greggi'],
        'Starbucks': ['Bill Greggi'],
      }
      Trialspark.setEmployeeThreshold(2)
      Trialspark.setRestaurantPreferences(restaurantPreferences)
      Trialspark.mapRestaurantsToEmployees(map)
      const equals = JSON.stringify(map) === JSON.stringify(mapResult)
      expect(equals).to.equal(true)
    })
  })

  describe ('findRestaurants', () => {
    it('should return a list of all pairs of restaurants that appear together in at least <threshold> different employees\' lists of preferred restaurants.', () => {
      const restaurantPreferences = {
        "John Smith": [
          "Chop't", "Strings", "In-N-Out", "Flacos Tacos", "Panera Bread", "Crazy 8's"
        ],
        "Frank Perkins": [
          "Chili's", "Chop't", "In-N-Out", "Burger King", "Five Guys"
        ],
        "Bill Greggi": [
          "Burger King", "Wendy's", "Chili's", "Starbucks", "Pizza Hut"
        ],
        "Carl Pilkington": [
          "Pizza Hut", "Blaze", "Flacos Tacos", "Strings", "Round Table Pizza"
        ],
        "Lou Richards": [
          "Chester's", "Chop't", "Crazy 8's", "Chipotle", "Panera Bread"
        ]
      }
      const restaurantPairsResult = [
        ['Chop\'t', 'In-N-Out'],
        ['Chop\'t', 'Panera Bread'],
        ['Chop\'t', 'Crazy 8\'s'],
        ['Strings', 'Flacos Tacos'],
        ['Panera Bread', 'Crazy 8\'s'],
        ['Chili\'s', 'Burger King']
      ]
      Trialspark.setEmployeeThreshold(2)
      Trialspark.setRestaurantPreferences(restaurantPreferences)
      const restaurantPairs = Trialspark.findRestaurants()
      expect(restaurantPairs).to.deep.equal(restaurantPairsResult)
    })
  })
})