'use strict'

const Trialspark = require('../related-restaurants')

/**
 * Example usage:
 * node index.js <employee threshold> <input JSON file>
 * node index.js 10 './restaurant_preferences_1k_employees.json'
 * node index.js 7 './restaurant_preferences.json'
**/
const employeeThreshold = process.argv[2]
const inputJsonFile = process.argv[3]

if (employeeThreshold === undefined || inputJsonFile === undefined) {
  console.error('Please pass in the proper command line arguments')
  return
}

Trialspark.setup(employeeThreshold, inputJsonFile)

console.time('finding related restaurants')
Trialspark.findRestaurantsAndOutputToFileAsJSON()
console.timeEnd('finding related restaurants')
