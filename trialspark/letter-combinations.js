/*
  Trialspark
  Q: Given a digit string, return all possible letter combinations that the number could represent on a T9 dialpad.
  Input: Digit string "23"
  Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"]
  https://leetcode.com/problems/letter-combinations-of-a-phone-number/description/
*/

const getPossibilities = digitString => {
  const digitMap = {
    2: 'abc',
    3: 'def',
    4: 'ghi',
    5: 'jkl',
    6: 'mno',
    7: 'pqrs',
    8: 'tuv',
    9: 'wxyz'
  }

  let possibilities = ['']

  for (let i = 0; i < digitString.length; i++) {
    let digit = digitString[i]
    let letters = digitMap[digit]
    let temp = []

    possibilities.forEach(possibility => {

      for (let j = 0; j < letters.length; j++) {
        let letter = letters[j]
        temp.push(possibility + letter)
      }
    })
    possibilities = temp.slice()
    temp = []
  }
  return possibilities
}

console.log(getPossibilities('234'))
// => [ 'adg', 'adh', 'adi', 'aeg', 'aeh', 'aei', 'afg', 'afh', 'afi', 'bdg', 'bdh', 'bdi', 'beg', 'beh', 'bei', 'bfg', 'bfh', 'bfi', 'cdg', 'cdh', 'cdi', 'ceg', 'ceh', 'cei', 'cfg', 'cfh', 'cfi' ]