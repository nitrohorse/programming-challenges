// Andreas Basurto
// Top Four
// 9/10 testcases passed
// https://evernotechallenge.interviewstreet.com/challenges/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
    int total       = 0;
    int read_num    = 0;
    int first       = 0;
    int second      = 0;
    int third       = 0;
    int fourth      = 0;
    vector<int> vec;

    cin >> total;
    total -= 4;
    cin >> first >> second >> third >> fourth;
    vec.push_back(first);
    vec.push_back(second);
    vec.push_back(third);
    vec.push_back(fourth);

    // sort in descending order
    sort(vec.begin(), vec.end(), greater<int>());

    first   = vec[0];
    second  = vec[1];
    third   = vec[2];
    fourth  = vec[3];

    while (total--)
    {
        cin >> read_num;
        if (read_num > first)
        {
            fourth  = third;
            third   = second;
            second  = first;
            first   = read_num;
        }
        else if ((read_num > second) && (read_num < first))
        {
            fourth  = third;
            third   = second;
            second  = read_num;
        }
        else if ((read_num > third) && (read_num < second))
        {
            fourth  = third;
            third   = read_num;
        }
        else if ((read_num > fourth) && (read_num < third))
        {
            fourth  = read_num;
        }
    }
    cout << first << endl
        << second << endl
        << third << endl
        << fourth;

    return 0;
}