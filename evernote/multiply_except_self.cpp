// Andreas Basurto
// Multiply Except Self
// 10/10 testcases passed
// https://evernotechallenge.interviewstreet.com/challenges/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
  vector<int> input_array;
  int num_elements = 0;
  int num = 0;
  cin >> num_elements;
  input_array.reserve(num_elements);
  while (num_elements > 0) {
    cin >> num;
    input_array.push_back(num);
    num_elements--;
  }
  long long mult = 1;
  int size = (int)(input_array.size());
  for (int i = 0; i < size; ++i) {
    for (int j = 0; j < size; ++j) {
      if (j != i)
        mult *= input_array[j];
    }
    cout << mult << endl;
    mult = 1;
  }
  return 0;
}