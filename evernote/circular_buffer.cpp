// Andreas Basurto & Shane Steuteville
// Circular Buffer
// 2/10 testcases passed
// https://evernotechallenge.interviewstreet.com/challenges/

#include <iostream>
#include <cctype>
#include <vector>
#include <string>

using namespace std;

int increment(int num, int size)
{
  num = (num + 1) % size;
  return num;
}

int main()
{
  int newest          = 0;
  int oldest          = 0;
  int num_lines       = 0;
  int num_remove      = 0;
  int size_of_buffer  = 0;
  int i               = 0;
  int count           = 0;
  string element      = "";
  char command        = 'A';
  bool empty          = true;

  vector<string> circular_buffer;

  cin >> size_of_buffer;
  circular_buffer.resize(size_of_buffer);

  while (command != 'Q')
  {
    cin >> command;
    command = toupper(static_cast<char>(command));

    switch (command)
    {
      case 'A':
      {
        cin >> num_lines;
        for (i = 0; i < num_lines; ++i)
        {
          cin >> element;
          if (empty)
          {
            circular_buffer[oldest] = element;
            empty = false;
          }
          else
          {
            newest = increment(newest, size_of_buffer);
            if (newest == oldest)
            {
              oldest = increment(oldest, size_of_buffer);
            }
            circular_buffer[newest] = element;
          }
        }
        break;
      }

      case 'R':
      {
        if (!empty)
        {
          cin >> num_remove;
          for(i = 0; i < num_remove; ++i)
          {
            if (oldest == newest)
            {
              circular_buffer[oldest] = "";
              empty = true;
            }
            else
            {
              circular_buffer[oldest] = "";
              oldest = increment(oldest, size_of_buffer);
            }
          }
        }
        break;
      }

      case 'L':
      {
        int j = oldest;
        count = 0;
        while (count < size_of_buffer && !empty)
        {
          cout << circular_buffer[j] << endl;
          j = increment(j, size_of_buffer);
          count++;
        }
        break;
      }

      case 'Q':
      {
        break;
      }

      default:
      {
        break;
      }
    }
  }
  return 0;
}