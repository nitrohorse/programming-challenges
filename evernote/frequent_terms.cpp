// Andreas Basurto
// Frequent Terms
// 10/10 testcases passed
// https://evernotechallenge.interviewstreet.com/challenges/

#include <iostream>
#include <map>
#include <vector>
#include <algorithm>

using namespace std;

bool descending_order(pair<string, int> i, pair<string, int> j)
{
    return j.second < i.second;
}

int main()
{
    int total_terms = 0;
    int most_frequent_terms = 0;
    int m_total_terms = 0;
    int count = 0;
    string term = "";
    map<string, int> terms_map;

    cin >> total_terms; // read in N terms
    m_total_terms = total_terms;

    while (total_terms--)
    {
        cin >> term;
        terms_map[term] += 1; // insert each term into hash
    }

    // to sort map of keys=>values based on value, instantiate pair vector
    // and then sort
    vector<pair<string, int> > terms_vec(terms_map.begin(), terms_map.end());
    stable_sort(terms_vec.begin(), terms_vec.end(), descending_order);

    cin >> most_frequent_terms; // read in k

    for (vector<pair<string, int> >::iterator i = terms_vec.begin();
        i != terms_vec.end(); ++i)
    {
        if (count < most_frequent_terms)
        {
            cout << i->first << endl;
            count++;
        }
        else break;
    }
    return 0;
}