/*
ID: andreas25
LANG: C++
TASK: beads
*/
#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

int main() 
{
	ifstream ifs("beads4.in");
	ofstream ofs("beads.out");

	string necklace = "";
	int count = 0;
	ifs >> count >> necklace;
	necklace.append(necklace); 

	int red_right_count = 0;
	int red_left_count = 0;
	int red_right_max = 0;
	int red_left_max = 0;

	int blue_right_count = 0;
	int blue_left_count = 0;
	int blue_right_max = 0;
	int blue_left_max = 0;

	char color_for_white_streak;
	bool get_first_white_bead = true;

	char prev_color = '0';

	cout << "\nThe necklace, doubled: " << necklace << endl;

	for (int i = 0; i < necklace.length(); ++i)
	{
		switch(necklace[i]) 
		{
			case 'r':
				if (prev_color == 'b')
					blue_right_count = 0;	
				if (prev_color == 'w')
				{
					blue_right_count = 0;
					get_first_white_bead = true;
				}
				red_right_count++;
				if (red_right_count > red_right_max)
					red_right_max = red_right_count;
				break;
			case 'b':
				if (prev_color == 'r')
					red_right_count = 0;
				if (prev_color == 'w')
				{
					red_right_count = 0;
					get_first_white_bead = true;
				}
				blue_right_count++;
				if (blue_right_count > blue_right_max)
					blue_right_max = blue_right_count;
				break;
			case 'w':
				if (get_first_white_bead)
				{
					if (prev_color == 'r')
						color_for_white_streak = 'r';
					else if (prev_color == 'b')
						color_for_white_streak = 'b';
					else // if prev_color == '0'
						color_for_white_streak = '0';
					get_first_white_bead = false;
				}
				if (color_for_white_streak == 'r')
					red_right_count++;
				else if (color_for_white_streak == 'b')
					blue_right_count++;
				else // if color_for_white_streak == '0'
				{
					red_right_count++;
					blue_right_count++;
				}
				if (red_right_count > red_right_max)
					red_right_max = red_right_count;
				if (blue_right_count > blue_right_max)
					blue_right_max = blue_right_count;
				break;
		}
		prev_color = necklace[i];
	}

	cout << "Red's max (going right): " << red_right_max << endl;
	cout << "Blue's max (going right):" << blue_right_max << endl;
	return 0;
}



/*
for(int i = 0; i < necklace.length(); ++i) 
	{
		switch(necklace[i])
		{
			case 'r':
				r_count++;
				break;
			case 'b':
				b_count++;
				break;
			case 'w':
				r_count++;
				b_count++;
				break;
			default:
				cout<<"Error (1)"<<endl;
				break;
		}		
		if (cur!=prev) {
			switch(cur) {
				case 'r':
					left_total=b_count;
					r_count=0;
					break;
				case 'b':
					left_total=r_count;
					b_count=0;
					break;
				default:
					cout<<"Error(2)"<<endl;
					break;
			}
		}


	}
*/

/*switch(necklace[i]){
			case 'r':
				red++;
				break;
			case 'b':
				blue++;
				break;
			case 'w':
				red++;
				blue++;
				break;
			default:
				cout<<"error1"<<endl;
				break;
		}
		if(cur==prev||cur=='w'){
			switch(cur){
				case 'r':
					red++;
					break;
				case 'b':
					blue++;
					break;
				case 'w':
					red++;
					blue++;
					break;ls
				default:
					cout<<"error2"<<endl;
					break;
			}
			total=max(blue+red,total);
		} else {
			switch(cur){
				case 'r':
					blue=0;
					break;
				case 'b':
					red=0;
					break;
				default:
					cout<<"error3"<<endl;
					break;
			}
		}
		prev=cur;
		cur=necklace[i++];
	}
	// cout<<"r_max: "<<r_max<<endl<<"b_max: "<<b_max<<endl;
	*/

	/*		switch(necklace[i+j]){
				case 'r':
					red_right++;
					break;
				case 'b':
					blue_right++;
					break;
				case 'w':
					red_right++;
					blue_right++;
					break;
			}
			switch(necklace[necklace.length()-j]){
				case 'r':
					red_left++;
					break;
				case 'b':
					blue_left++;
					break;
				case 'w':
					red_left++;
					blue_left++;
					break;				
			}
		}
		if (red_left>red_left_max)
			red_left_max=red_left;
		if (blue_left>blue_left_max)
			blue_left_max=blue_left;
		if (red_right>red_right_max)
			red_right_max=red_right;
		if (blue_right>blue_right_max)
			blue_right_max=blue_right;
		red_left=0;
		red_right=0;
		blue_left=0;
		blue_right=0;*/


/*

	list<char> beads (necklace.begin(), necklace.end());
	list<char> beads_again (beads);

	list<char> circular_list;

	circular_list = beads + beads_again;
	
	list<char>::iterator break_point = beads.begin();
	list<char>::iterator f_iter = break_point;
	list<char>::iterator r_iter = break_point;

	for (; break_point != beads.end(); ++break_point)
	{
		char forward_color = *f_iter;
		char reverse_color = *r_iter;

		int forward_char_count = 0;
		int reverse_char_count = 0;

		while(*f_iter == forward_color || *f_iter == 'w')
		{
			*f_iter++;
			*r_iter++;
			forward_char_count++;

		}
		while(*r_iter == reverse_color || *r_iter == 'w')
		{
			*f_iter++;
			*r_iter++;
			reverse_char_count++;
		}
	}
*/






	/*

	int reverse_iterator = (forward_iterator + necklace.length() - 1) 
		% necklace.length();

	for (int i=0; i < necklace.length(); 
		forward_iterator++, reverse_iterator++, i++)
	{
		int forward_char_count = 0;
		int reverse_char_count = 0;

		char forward_color = necklace[forward_iterator];
		char reverse_color = necklace[reverse_iterator];

		cout << "Break at " << i << ": ";

		cout << "forward ";
		while (necklace[forward_iterator] == forward_color 
			|| necklace[forward_iterator] == 'w')
		{
			cout << necklace[forward_iterator] << " ";
			forward_char_count++;
			forward_iterator++;
			forward_iterator %= necklace.length();
			if (forward_char_count == necklace.length()) 
				break;
		}


		cout << "reverse ";
		while (necklace[reverse_iterator] == reverse_color 
			|| necklace[reverse_iterator] == 'w')
		{
			cout << necklace[reverse_iterator] << " ";
			reverse_char_count++;
			reverse_iterator--;
			reverse_iterator %= necklace.length();
			if (reverse_char_count == necklace.length()) 
				break;
		}
		cout << endl;
		forward_iterator %= necklace.length();
		reverse_iterator = (forward_iterator + necklace.length() - 1) % necklace.length();
		if (forward_char_count + reverse_char_count > max_consec_beads)
			max_consec_beads = forward_char_count + reverse_char_count;
	}
	*/
