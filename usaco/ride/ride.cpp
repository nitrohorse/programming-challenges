/*
ID: andreas25
LANG: C++
TASK: ride
*/

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() 
{
    ifstream in("ride.in");
    ofstream out("ride.out");
    string groupName = "";
    string cometName = "";
    int groupNum = 1;
    int cometNum = 1;
    getline (in, cometName);
    getline (in, groupName);
    int gLength = groupName.length();
    int cLength = cometName.length();
    
    for (int i = 0; i < gLength; ++i)
        groupNum *= (groupName[i] - 64); 
    
    for (int i = 0; i < cLength; ++i)
        cometNum *= (cometName[i] - 64);

    if ((groupNum % 47) == (cometNum % 47)) // Get ready!!
        out << "GO" << endl;
    else 
        out << "STAY" << endl;
    return 0;
}
