/*
ID: andreas25
LANG: C++
TASK: gift1
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

using namespace std;

int main() 
{
    ifstream in("gift1.in");
    ofstream out("gift1.out");
    int NP = 0, total, people;
    vector<string> v;
    map<string,int> accounts;

    in >> NP;

    int counter = NP;
    string s, c;
    while (counter--) 
    {
        in >> s;
        v.push_back(s);
        accounts[s] = 0;
    }

    counter = NP;
    while (counter--) 
    {
        in >> s >> total >> people;
        if (people > 0) 
        {
            accounts[s] -= (total - (total % people));
            for (int i = 0, amt = total/people; i < people; ++i) 
            {
                in >> c;
                accounts[c] += amt;
            }
        }
    }

    for(int i = 0; i < v.size(); ++i)
        out << v[i] << " " << accounts[v[i]] << endl;

    return 0;
}