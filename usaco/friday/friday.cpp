/*
ID: andreas25
LANG: C++
TASK: friday
*/

#include <iostream>
#include <fstream>

using namespace std;

bool isLeapYear(int year) {
    return (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
}

int main() {
    ifstream in("friday.in");
    ofstream out("friday.out");
    int n_years;
    in >> n_years;

    int year_arr[12] = {31, 28, 31, 30, 31, 30,
                        31, 31, 30, 31, 30, 31};

    int leap_year_arr[12] = {31, 29, 31, 30, 31, 30, 
                             31, 31, 30, 31, 30, 31};

    int day_13[7] = {0,0,0,0,0,0,0};

    bool leap_year = false;
    int day_of_week = 0;

    for (int year = 0; year < n_years; ++year) { // YEARS
        leap_year = isLeapYear(1900 + year); // if a leap year
        for (int month = 0; month < 12; ++month) { // MONTHS
            if (!leap_year) {
                for (int day = 0; day < year_arr[month]; 
                    day_of_week = (day_of_week < 6) ? day_of_week + 1 : 0, ++day) 
                    if (day == 12)
                        day_13[day_of_week]++;
            }
            else {
                for (int day = 0; day < leap_year_arr[month]; 
                    day_of_week = (day_of_week < 6) ? day_of_week + 1 : 0, ++day) 
                    if (day == 12) day_13[day_of_week]++;
            }
        }
    }
    for (int i = 0; i < 7; ++i) {
        out << day_13[(i + 5) % 7];
        if (i != 6) out << " ";
    }

    out << endl;
    return 0;
}