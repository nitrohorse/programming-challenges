'use strict'

require('google-closure-library')
goog.require('goog.structs.Heap')

const minHeap = new goog.structs.Heap()

module.exports = (logSources, printer) => {
	for (let i = logSources.length - 1; i >= 0; i--) {
		const logEntry = logSources[i].pop()
		minHeap.insert(logEntry.date, logEntry)
	}

	while (minHeap.nodes_.length) {
		printer.print(minHeap.remove())
	}
	printer.done()
}