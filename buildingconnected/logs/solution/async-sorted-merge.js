'use strict'

require('google-closure-library')
goog.require('goog.structs.Heap')

const minHeap = new goog.structs.Heap()

module.exports = (logSources, printer) => {
	(async () => {
		try {
			const logEntryPromises = logSources.map(logSource => logSource.popAsync())
			const logEntries = await Promise.all(logEntryPromises)

			for (let i = logEntries.length - 1; i >= 0; i--) {
				const logEntry = logEntries[i]
				minHeap.insert(logEntry.date, logEntry)
			}

			while (minHeap.nodes_.length) {
				printer.print(minHeap.remove())
			}
			printer.done()
		} catch (err) {
			console.error(err)
		}
	})()
}