/* 
Create an application that prints all the words in a Boggle board represented by a 2D matrix.
  * Input: Board represented as 2D matrix.
  * Input: Set of valid words.
  * Output: Words contained in the boggle board.
  * Notes:
    * Can’t re-use the same letter for a given word.
    * Letters can’t be connected diagonally.
    * Print the word every time it appears
    * Runnable solution

Board:
[
  ['d', 'b', 'c'],
  ['a', 'c', 'a'],
  ['b', 'a', 't']
]

Set of valid words:
'cab',
'cat',
'bat',

Output:
'cab'
'cab'
'cat'
'cat'
'cat'
'bat'
 */

const log = console.log

const isValidWord = (potentialWord, validWords) => (validWords.indexOf(potentialWord) !== -1)

const findWords = (board, row, column, potentialWord, visited, validWords) => {
  const size = board.length
  if (row < 0 || row >= size || column < 0 || column >= size || visited[row][column]) {
    return
  }

  visited[row][column] = true

  potentialWord += board[row][column]

  if (isValidWord(potentialWord, validWords)) {
    log(potentialWord)
  }

  findWords(board, row + 1, column, potentialWord, visited, validWords) // down
  findWords(board, row - 1, column, potentialWord, visited, validWords) // up
  findWords(board, row, column + 1, potentialWord, visited, validWords) // right
  findWords(board, row, column - 1, potentialWord, visited, validWords) // left

  potentialWord = potentialWord.slice(0, -1)
  visited[row][column] = false
}

const boggleSolver = (board, validWords) => {
  const size = board.length

  const visited = new Array(size)
  for (let index = 0; index < size; index++) {
    visited[index] = new Array(size).fill(false)
  }

  let potentialWord = ''

  for (let i = 0; i < size; i++) {
    for (let j = 0; j < size; j++) {
      findWords(board, i, j, potentialWord, visited, validWords)
    }
  }
}

const board = [
  ['d', 'b', 'c'],
  ['a', 'c', 'a'],
  ['b', 'a', 't']
]

const validWords = ['cab', 'cat', 'bat',]

boggleSolver(board, validWords)
/*
cat
cat
cab
cat
cab
bat
*/