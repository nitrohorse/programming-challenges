/*
  Complexity
    O(1) time:
      For ensuring each method runs in constant time, the calculations for max, min, mean, and mode are performed upon tracking a new integer so that the getters become a constant time lookup.
    O(n) space:
      For each new integer encountered, it is added to the map, numberOfOccurances, which counts how many times an integer has been tracked. N is dependent on the uniqueness of the input; worse case being all integers tracked are unique and thus added to the map. The map helps us get the mode in constant time.
*/
class IntegerTracker {
  constructor() {
    this.max = null
    this.min = null
    this.mean = null
    this.mode = null

    this.sumOfIntegers = 0
    this.numberOfIntegers = 0
    this.numberOfOccurances = new Map()
    this.maxNumberOfOccurances = null
  }

  track(int) {
    this.max = Math.max(this.max, int)
    this.min = Math.min(this.min, int)

    this.sumOfIntegers += int
    this.numberOfIntegers++
    this.mean = this.sumOfIntegers / this.numberOfIntegers

    // track all integers seen so far for mode
    if (!this.numberOfOccurances.get(int)) {
      this.numberOfOccurances.set(int, 1)
    } else {
      const occurances = this.numberOfOccurances.get(int)
      this.numberOfOccurances.set(int, occurances + 1)
    }

    if (this.numberOfOccurances.get(int) > this.maxNumberOfOccurances) {
      this.mode = int
      this.maxNumberOfOccurances = this.numberOfOccurances.get(int)
    }
  }

  get_max() {
    return this.max
  }

  get_min() {
    return this.min
  }

  get_mean() {
    return this.mean
  }

  get_mode() {
    return this.mode
  }
}

const it = new IntegerTracker()
it.track(1)
it.track(0)
console.log('max:', it.get_max()) // => max: 1
console.log('min:', it.get_min()) // => min: 0
console.log('mean:', it.get_mean()) // => mean: 0.5
console.log('mode:', it.get_mode()) // => mode: 1
it.track(3)
it.track(1)
console.log('max:', it.get_max()) // => max: 3
console.log('min:', it.get_min()) // => min: 0
console.log('mean:', it.get_mean()) // => mean: 1.25
console.log('mode:', it.get_mode()) // => mode: 1
