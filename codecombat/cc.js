/*
  make data structure
    - cache
      (can be class or obj)
    - two methods
      - get(key) => return value
      - set(key, value) => set value to key
*/
const log = console.log

// key, value => arbitrary strings
class Cache {
  constructor(maxSize) {
    this.maxSize = maxSize
    this.cache = new Array() // new Map()
    // [[key, val], [key, val], [key, val]]
  }

  // Time complexity: O(n)
  get(key) {
    const indexOfKeyValuePair = this.cache.findIndex(pair => pair[0] === key)

    if (indexOfKeyValuePair !== -1) {
      // remove from current position
      const keyValuePair = this.cache.splice(indexOfKeyValuePair, 1)[0]

      // add element to front
      this.cache.unshift(keyValuePair)

      return keyValuePair[1]
    } else {
      return null
    }
  }

  // Time complexity: O(n)
  set(key, value) {
    const indexOfKeyValuePair = this.cache.findIndex(pair => pair[0] === key)

    if (indexOfKeyValuePair !== -1) {
      // remove from current position
      this.cache.splice(indexOfKeyValuePair, 1)

      // add element to front
      this.cache.unshift([key, value])
    } else {
      if (this.cache.length === this.maxSize) {
        this.cache.pop() // evict
      }
      // add element to front
      this.cache.unshift([key, value])
    }
  }
}
const maxSize = 3
const cache = new Cache(maxSize)
cache.set('andreas', 'dude')
cache.set('scott', 'tech-lead')
cache.set('barbara', 'sales')
cache.set('barry', 'system-engie')
log(cache.get('scott'))
// => tech-lead
log(cache.get('zeb'))
// => null

log(cache)

cache.set('barry', 'ceo')

log(cache)

// make cache into LRU cache
/*
  
  evict key/value pairs
  cache => max size (3)
  ['C', 'A', 'B'], 'D'
  D => ['D', 'C', 'A']
*/