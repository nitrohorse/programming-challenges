/*
Level:
  _id: unique level ID
  name (string)
  unlocks (array of strings): ids of the levels that follow this level
  first (boolean): whether the level is always available

User-Level State:
  _id: unique state ID
  level: ID of the level
  state: one of [‘complete’, ‘started’]

Return an array of names of levels the user has completed
*/

const simpleData = {
  "levels": [
    {
      "_id": "level_1",
      "name": "First Level",
      "unlocks": [
        "level_2"
      ],
      "first": true
    },
    {
      "_id": "level_2",
      "name": "Second Level",
      "unlocks": [],
      "first": false
    },
    {
      "_id": "level_3",
      "name": "Inaccessible Level",
      "unlocks": [],
      "first": false
    }
  ],
  "userLevelStates": [
    {
      "_id": "state_1",
      "level": "level_1",
      "state": "complete"
    }
  ]
}

const complexData = {
  "levels": [
    {
      "_id": "level_1",
      "name": "Corporate Frozen",
      "unlocks": [
        "level_2"
      ],
      "first": true
    },
    {
      "_id": "level_2",
      "name": "Handcrafted Steel Cheese Zambia",
      "unlocks": [
        "level_3",
        "level_12"
      ],
      "first": false
    },
    {
      "_id": "level_3",
      "name": "Rustic Metal",
      "unlocks": [
        "level_4"
      ],
      "first": false
    },
    {
      "_id": "level_4",
      "name": "primary Home",
      "unlocks": [
        "level_5"
      ],
      "first": false
    },
    {
      "_id": "level_5",
      "name": "Alaska Corporate",
      "unlocks": [
        "level_6"
      ],
      "first": false
    },
    {
      "_id": "level_6",
      "name": "Upgradable Indiana",
      "unlocks": [
        "level_7",
        "level_14",
        "level_19"
      ],
      "first": false
    },
    {
      "_id": "level_7",
      "name": "Tools challenge",
      "unlocks": [
        "level_8"
      ],
      "first": false
    },
    {
      "_id": "level_8",
      "name": "Dalasi Money Market Account",
      "unlocks": [
        "level_9"
      ],
      "first": false
    },
    {
      "_id": "level_9",
      "name": "strategize collaboration",
      "unlocks": [
        "level_10"
      ],
      "first": false
    },
    {
      "_id": "level_10",
      "name": "benchmark Cayman Islands Dollar",
      "unlocks": [
        "level_11"
      ],
      "first": false
    },
    {
      "_id": "level_11",
      "name": "FTP Extended",
      "unlocks": [],
      "first": false
    },
    {
      "_id": "level_12",
      "name": "Land invoice",
      "unlocks": [
        "level_13"
      ],
      "first": false
    },
    {
      "_id": "level_13",
      "name": "override Singapore Dollar",
      "unlocks": [],
      "first": false
    },
    {
      "_id": "level_14",
      "name": "Afghanistan Kids",
      "unlocks": [
        "level_15"
      ],
      "first": false
    },
    {
      "_id": "level_15",
      "name": "user-centric Engineer",
      "unlocks": [
        "level_16"
      ],
      "first": false
    },
    {
      "_id": "level_16",
      "name": "application Metal",
      "unlocks": [
        "level_17"
      ],
      "first": false
    },
    {
      "_id": "level_17",
      "name": "navigating deposit",
      "unlocks": [
        "level_18"
      ],
      "first": false
    },
    {
      "_id": "level_18",
      "name": "haptic Lakes",
      "unlocks": [],
      "first": false
    },
    {
      "_id": "level_19",
      "name": "Licensed Cotton Mouse HDD",
      "unlocks": [
        "level_20"
      ],
      "first": false
    },
    {
      "_id": "level_20",
      "name": "ability Strategist",
      "unlocks": [
        "level_22"
      ],
      "first": false
    },
    {
      "_id": "level_21",
      "name": "Architect background",
      "unlocks": [
        "level_22"
      ],
      "first": true
    },
    {
      "_id": "level_22",
      "name": "Spurs North Dakota",
      "unlocks": [
        "level_23"
      ],
      "first": false
    },
    {
      "_id": "level_23",
      "name": "transmit Maine",
      "unlocks": [
        "level_24"
      ],
      "first": false
    },
    {
      "_id": "level_24",
      "name": "withdrawal backing up",
      "unlocks": [
        "level_4"
      ],
      "first": false
    },
    {
      "_id": "level_25",
      "name": "optical Wooden",
      "unlocks": [],
      "first": false
    }
  ],
  "userLevelStates": [
    {
      "_id": "state_51",
      "level": "level_1",
      "state": "complete"
    },
    {
      "_id": "state_52",
      "level": "level_2",
      "state": "complete"
    },
    {
      "_id": "state_53",
      "level": "level_3",
      "state": "complete"
    },
    {
      "_id": "state_54",
      "level": "level_4",
      "state": "complete"
    },
    {
      "_id": "state_55",
      "level": "level_5",
      "state": "complete"
    },
    {
      "_id": "state_56",
      "level": "level_6",
      "state": "complete"
    },
    {
      "_id": "state_57",
      "level": "level_8",
      "state": "started"
    },
    {
      "_id": "state_58",
      "level": "level_12",
      "state": "complete"
    },
    {
      "_id": "state_59",
      "level": "level_13",
      "state": "complete"
    },
    {
      "_id": "state_60",
      "level": "level_14",
      "state": "complete"
    },
    {
      "_id": "state_61",
      "level": "level_15",
      "state": "complete"
    }
  ]
}

const log = console.log

const getArrayOfCompleteLevelNames = data => {
  const levels = data.levels
  const userLevelStates = data.userLevelStates

  const levelIdToNameMap = {}
  levels.forEach(level => {
    levelIdToNameMap[level._id] = level.name
  })

  const levelIds = new Array()
  userLevelStates.forEach(userLevelState => {
    if (userLevelState.state === 'complete') {
      levelIds.push(userLevelState.level)
    }
  })

  // const completeLevelNames = new Array()

  // levelIds.forEach(levelId => {
  //   completeLevelNames.push(levelIdToNameMap[levelId])
  // })

  // return completeLevelNames

  levelIds.forEach(levelId => {
    const level = levels.
  })

}

// log(getArrayOfCompleteLevelNames(simpleData))
// =>

log(getArrayOfCompleteLevelNames(complexData))
// =>

/*
  Return Ids of available levels to the user
*/