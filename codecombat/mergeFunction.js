/*
  create merge function
    - takes two objects and returns one merged object

    objects defined as:
      - native JS object -> keys/values
        - for duplicate keys, overwrite values

    follow-up:
      - add support for merging nested objects
        - if same key and both values are objects apply merge logic
*/
const log = console.log

const mergeObjects = (obj1, obj2) => {
  const mergedObj = {}

  Object.keys(obj1).forEach(key => {
    mergedObj[key] = obj1[key]
  })

  Object.keys(obj2).forEach(key => {
    if (mergedObj[key]) {
      // recursive case
      if (typeof mergedObj[key] === 'object' && typeof obj2[key] === 'object') {
        mergedObj[key] = mergeObjects(mergedObj[key], obj2[key])
      } else {
        // base case
        mergedObj[key] = obj2[key]
      }
    } else {
      mergedObj[key] = obj2[key]
    }
  })

  return mergedObj
}

let obj1 = {
  'B': 'Barry',
  'C': {
    'B': {
      'C': 'Clark'
    }
  }
}

let obj2 = {
  'B': 'Barney',
  'C': {
    'B': 'Barry'
  },
}

log('Test 1:', mergeObjects(obj1, obj2))
/* => 
{
  'B': 'Barney',
  'C': {
    'B': 'Barry'
  }
}
*/

obj1 = {
  'a': 'andreas',
  'b': 'barry',
  'c': {
    'd': 'dan',
    'e': 'elrond'
  }
}

obj2 = {
  'a': 'andreas',
  'b': 'barney',
  'c': {
    'd': 'dan',
    'e': {
      'g': 'greg',
      'h': 'harry'
    }
  }
}
log('Test 2:', mergeObjects(obj1, obj2))
/*
{
  'a': 'andreas',
  'b': 'barney',
  'c': {
    'd': 'dan',
    'e': {
      'f': 'frank',
      'g': 'greg',
      'h': 'harry'
    }
  }
}
*/