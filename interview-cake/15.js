/*
  Complexity
    O(n) time
    O(1) space
*/
// sequence: 0,1,1,2,3,5,8,13,21...
const fib = n => {
  if (n < 0) {
    throw new Error('Index must be greater than or equal to 0')
  } else if (n === 0 || n === 1) {
    return n
  }

  let prevPrev = 0
  let prev = 1
  let current

  for (let i = 1; i < n; i++) {
    current = prevPrev + prev
    prevPrev = prev
    prev = current
  }
  return current
}

console.log(fib(4))
// => 3