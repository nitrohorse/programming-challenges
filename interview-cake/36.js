const getRandom = (floor, ceiling) => {
  return Math.floor(Math.random() * (ceiling - floor + 1)) + floor;
}

const riffle = deck => {
  let middle = Math.floor(deck.length / 2)
  let half1 = deck.slice(0, middle)
  let half2 = deck.slice(middle)

  // console.log('half 1:', half1)
  // console.log('half 2:', half2)

  let shuffledDeck = new Array()

  while (half1.length || half2.length) {
    let randomNumberFromHalf2 = getRandom(0, half2.length - 1)
    let randomNumberFromHalf1 = getRandom(0, half1.length - 1)

    console.log('random # from half 1:', randomNumberFromHalf1)
    console.log('random # from half 2:', randomNumberFromHalf2)

    let slice1 = half1.splice(-randomNumberFromHalf1)
    let slice2 = half2.splice(-randomNumberFromHalf2)

    console.log('slice 1:', slice1)
    console.log('\thalf 1:', half1)
    console.log('slice 2:', slice2)
    console.log('\thalf 2:', half2)

    shuffledDeck = shuffledDeck.concat(slice1).concat(slice2)
  }

  console.log('shuffled deck:', shuffledDeck)


}

const deck = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(riffle(deck))