const findLargest = root => {
  let current = root
  while (current) {
    if (!current.right) {
      return current.value
    }
    current = current.right
  }
}

/*
  Complexity
    O(h) time - height of tree
      O(log n) if balanced, O(n) otherwise
    O(1) space
*/
const findSecondLargest = root => {
  if (!root || (!root.left && !root.right)) {
    throw new Error('Tree must have at least 2 nodes')
  }

  let current = root

  while (current) {
    // case: we're currently at largest, and largest has a left subtree,
    // so 2nd largest is largest in said subtree
    if (current.left && !current.right) {
      return findLargest(current.left)
    }
    // case: we're at parent of largest, and largest has no left subtree,
    // so 2nd largest is current node
    if (current.right && !current.right.left && !current.right.right) {
      return current.value
    }
    // otherwise: step right
    current = current.right
  }
}

const inOrderTraversal = (root, values) => {
  if (root) {
    inOrderTraversal(root.left, values)
    values.push(root.value)
    inOrderTraversal(root.right, values)
  }
}

/*
  Complexity
    O(n) time
    O(n) space
*/
const isValid = root => {
  const values = []
  inOrderTraversal(root, values)
  let isValid = true
  for (let i = 0; i < values.length; i++) {
    if (values[i] > values[i + 1]) {
      isValid = false
      break
    }
  }
  return isValid
}

class BinaryTreeNode {
  constructor (value) {
    this.value = value
    this.left = null
    this.right = null
  }

  insertLeft (value) {
    this.left = new BinaryTreeNode(value)
    return this.left
  }

  insertRight (value) {
    this.right = new BinaryTreeNode(value)
    return this.right
  }
}

/*
  Complexity
    O(n) time
    O(n) space
*/
const isSuperBalanced = root => {
  if (!root) {
    return true
  }

  const depths = []
  const nodes = []
  nodes.push([root, 0])

  while (nodes.length) {
    let nodePair = nodes.pop()
    let node = nodePair[0]
    let depth = nodePair[1]

    // case: we found a leaf
    if (!node.left && !node.right) {

      // we only care if it's a new depth
      if (depths.indexOf(depth) === -1) {
        depths.push(depth)

        // two ways we might now have an unbalanced tree:
        //  1) more than 2 different leaf depths
        //  2) 2 leaf depths that are more than 1 apart
        if ((depths.length > 2)
        || (depths.length === 2 && Math.abs(depths[0] - depths[1] > 1))) {
          return false
        }
      }

    // case: this isn't a leaf - keep stepping down
    } else {
      if (node.left) {
        nodes.push([node.left, depth + 1])
      }
      if (node.right) {
        nodes.push([node.right, depth + 1])
      }
    }
  }
  return true
}



const bt = new BinaryTreeNode(24)
bt.left = new BinaryTreeNode(20)
bt.right = new BinaryTreeNode(30)
// bt.right.left = new BinaryTreeNode(26)
bt.left.left = new BinaryTreeNode(7)
bt.left.right = new BinaryTreeNode(23)
bt.left.left.left = new BinaryTreeNode(3)
bt.left.left.left.right = new BinaryTreeNode(5)

// console.log(bt)
console.log('2nd largest item:', findSecondLargest(bt))
console.log('Is valid?', isValid(bt))
console.log('Is super balanced?', isSuperBalanced(bt))
