/*
  Complexity
    O(log n) time
    O(1) space
*/
const findIndexOfRotationPoint = array => {
  let low = 0
  let high = array.length - 1

  while (low < high) {
    let middle = low + Math.floor((high - low) / 2)

    if (array[middle] > array[low]) {
      // go right
      low = middle
    } else {
      // go left
      high = middle
    }

    if (low + 1 === high) {
      break
    }
  }
  return high
}

const words = [
  'ptolemaic',
  'quizly',
  'retrograde',
  'supplant',
  'tooty',
  'undulate',
  'xenoepist',
  'asymptote',  // <-- rotates here!
  'babka',
  'banoffee',
  'engender',
  'karpatka',
  'othellolagkage',
];

console.log('Index of rotation point:', findIndexOfRotationPoint(words))
// => 7