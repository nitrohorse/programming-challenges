/*
  Complexity
    O(n) time
    O(n) space
*/
const isValid = string => {
  let stack = []

  for (let i = 0; i < string.length; i++) {
    const char = string[i]

    if (char === '{' || char === '[' || char === '(') {
      stack.push(char)
    } else {
      if (stack.length === 0) {
        return false
      } else {
        let topOfStack = stack[stack.length - 1]
        if (char === '}' && topOfStack === '{'
          || char === ']' && topOfStack === '['
          || char === ')' && topOfStack === '(') {
          stack.pop()
        } else {
          return false
        }
      }
    }
  }
  return true
}

console.log(isValid('{[]()}'))
// => true

console.log(isValid('{[(])}'))
// => false

console.log(isValid('{[}'))
// => false