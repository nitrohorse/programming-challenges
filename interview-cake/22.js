class LinkedListNode {
  constructor(value) {
    this.value = value
    this.next = null
  }
}

const print = head => {
  let current = head

  while (current) {
    console.log(`${current.value}->`)
    current = current.next
  }
}

const deleteNode = nodeToDelete => {
  // if node to delete is the tail
  if (!nodeToDelete.next) {
    nodeToDelete = null
    return
  }

  let next = nodeToDelete.next
  // override
  nodeToDelete.value = next.value

  // fix pointer
  nodeToDelete.next = next.next

  delete next
}

/*
  Given a sorted linked list, delete all duplicates such that each element appear only once.

  For example,
  Given 1->1->2, return 1->2.
  Given 1->1->2->3->3, return 1->2->3.
*/
const deleteDuplicates = head => {
  let current = head
  let runner = head

  while (current) {
    while (runner && runner.value === current.value) {
      runner = runner.next
    }
    current.next = runner
    current = runner
  }
}

const removeElements = (head, val) => {
  if (!head) {
    return -1
  }

  while (head && head.value === val) {
    head = head.next
  }

  let current = head

  while (current && current.next) {
    if (current.next.value === val) {
      current.next = current.next.next
    } else {
      current = current.next
    }
  }
  return head
}

const a = new LinkedListNode('D')
const b = new LinkedListNode('D')
const c = new LinkedListNode('B')

a.next = b
b.next = c

// const a = new LinkedListNode('A')
// const b = new LinkedListNode('D')
// const c = new LinkedListNode('B')
// const d = new LinkedListNode('D')
// const e = new LinkedListNode('D')
// const f = new LinkedListNode('E')

// a.next = b
// b.next = c
// c.next = d
// d.next = e
// e.next = f

print(a)
console.log('-----------------')
// deleteDuplicates(a)
// deleteNode(a)
console.log(removeElements(a, 'D'))
print(a)