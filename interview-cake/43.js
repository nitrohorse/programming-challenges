const mergeSortedArrays = (array1, array2) => {
  let array1Copy = array1.slice()
  let array2Copy = array2.slice()
  let mergedArray = new Array()

  while (array1Copy.length && array2Copy.length) {
    let frontOfArray1 = array1Copy[0]
    let frontOfArray2 = array2Copy[0]

    if (frontOfArray1 < frontOfArray2) {
      mergedArray.push(array1Copy.shift())
    } else {
      mergedArray.push(array2Copy.shift())
    }
  }

  if (array1Copy.length) {
    mergedArray = mergedArray.concat(array1Copy)
  } else if (array2Copy.length) {
    mergedArray = mergedArray.concat(array2Copy)
  }

  return mergedArray
}


const myArray = [3, 4, 6, 10, 11, 15]
const alicesArray = [1, 5, 8, 12, 14, 19]

console.log(mergeSortedArrays(myArray, alicesArray))
// => [1, 3, 4, 5, 6, 8, 10, 11, 12, 14, 15, 19]