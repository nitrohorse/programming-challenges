/*
  Complexity
    O(n) time
    O(1) space
*/
const permutationPalindrome = string => {
  let unpairedCharacters = new Set()

  for (let i = 0; i < string.length; i++) {
    const char = string[i]
    if (unpairedCharacters.has(char)) {
      unpairedCharacters.delete(char)
    } else {
      unpairedCharacters.add(char)
    }
  }
  return unpairedCharacters.size <= 1
}

console.log(permutationPalindrome('iivcc'))
// => true