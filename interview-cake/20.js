class Stack {
  constructor() {
    this.items = []
  }

  push(item) {
    this.items.push(item)
  }

  pop() {
    if (!this.items.length) {
      return null
    }
    return this.items.pop()
  }

  peek() {
    if (!this.items.length) {
      return null
    }
    return this.items[this.items.length - 1]
  }
}
/*
  Complexity
    O(1) time
    O(m) space where m is the number of operations performed on the stack
*/
class MaxStack {
  constructor() {
    this.items = new Stack()
    this.maxItems = new Stack()
  }

  push(item) {
    this.items.push(item)
    if (!this.maxItems.peek() || item >= this.maxItems.peek()) {
      this.maxItems.push(item)
    }
  }

  pop() {
    const item = this.items.pop()
    if (item === this.maxItems.peek()) {
      this.maxItems.pop()
    }
    return item
  }

  getMax() {
    return this.maxItems.peek()
  }
}

const ms = new MaxStack()
ms.push(3)
ms.push(2)
ms.push(1)
console.log('max stack:', ms.maxItems)
console.log('stack:', ms.items)
ms.pop()
console.log('max stack:', ms.maxItems)
console.log('stack:', ms.items)