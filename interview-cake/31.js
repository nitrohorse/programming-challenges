const getPermutations = string => {
  // base case
  if (string.length <= 1) {
    return new Set(string)
  }

  let allCharsExceptFirstChar = string.slice(1)
  let permutationsOfCharsExceptFirstChar = getPermutations(allCharsExceptFirstChar)

  let firstChar = string[0]
  let permutations = new Set()

  permutationsOfCharsExceptFirstChar.forEach(permutationOfCharsExceptFirst => {
    for (let position = 0; position <= allCharsExceptFirstChar.length; position++) {
      let permutation =
        permutationOfCharsExceptFirst.slice(0, position)
        + firstChar + permutationOfCharsExceptFirst.slice(position)
      permutations.add(permutation)
    }
  })
  return permutations
}

console.log(getPermutations('ABC'))
// => Set { 'CBA', 'BCA', 'BAC', 'CAB', 'ACB', 'ABC' }