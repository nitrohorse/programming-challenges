/*
  Complexity
    O(n) time
    O(1) space
*/
const countParentheticals = (pos, string) => {
  let countOfOpenParens = 0
  for (let i = 0; i < string.length; i++) {
    const char = string[i];
    if (i <= pos && char === '(') {
      countOfOpenParens++
    }
  }
  console.log(`Count of open parens seen up to ${pos}: ${countOfOpenParens}`)

  let posOfClosingParen = 0

  for (let i = string.length - 1; i >= 0; i--) {
    const char = string[i]
    if (char === ')') {
      countOfOpenParens -= 1
      if (countOfOpenParens === 0) {
        posOfClosingParen = i
      }
    }
  }
  console.log(`Index of closing paren: ${posOfClosingParen}`)
  return posOfClosingParen
}

let string = 'Sometimes (when I nest them (my parentheticals) too much (like this (and this))) they get confusing.'
console.log(countParentheticals(10, string))