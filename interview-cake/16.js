const maxDuffelBagValue = (cakeTypes, capacity) => {
  // array to hold the maximum possible value at every integer capacity from
  // 0 to capacity starting each index with value 0
  let maxValuesAtCapacities = new Array(capacity).fill(0)
  
  for (let currentCapacity = 0; currentCapacity <= capacity; currentCapacity++) {
    // set a variable to hold the max monetary value so far for currentCapacity
    let currentMaxValue = 0

    for (let j = 0; j < cakeTypes.length; j++) {
      const cakeType = cakeTypes[j]

      // if a cake weighs 0 and has a positive value the value of our duffel bag
      // is infinite!
      if (cakeType.weight === 0 && cakeType.value !== 0) {
        return Infinity
      }

      // if the cake weighs as much or less than the current capacity
      // see what our max value could be if we took it!
      if (cakeType.weight <= currentCapacity) {
        // so we check: should we use the cake or not?
        // if we use the cake, the most kilograms we can include in addition to
        // the cake we're adding is the current capacity minus the cake's weight.
        // We find the max value at the integer capacity in our array
        // maxValuesAtCapacities.
        let maxValueUsingCake = cakeType.value + maxValuesAtCapacities[currentCapacity - cakeType.weight]

        // now we see if it's worth taking the cake. how does the value with the
        // cake compare to the currentMaxValue?
        currentMaxValue = Math.max(maxValueUsingCake, currentMaxValue)
      }
    }

    // add each capacity's max value to our array so we can use them when
    // calculating all the remaining capacities
    maxValuesAtCapacities[currentCapacity] = currentMaxValue
  }
  return maxValuesAtCapacities[capacity]
}

const cakeTypes = [
  { weight: 7, value: 160 },
  { weight: 3, value: 90 },
  { weight: 2, value: 15 },
]

const capacity = 20

console.log(maxDuffelBagValue(cakeTypes, capacity))
// => 555 (6 of the middle type of cake and 1 of the last type of cake)