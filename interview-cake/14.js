/*
  Complexity
    O(n) time
    O(n) space
*/
const areThereTwoMoviesForFlight = (flightLength, movieLengths) => {
  let map = []

  for (let i = 0; i < movieLengths.length; i++) {
    const movieLength = movieLengths[i]

    if (map[flightLength - movieLength]) {
      return true
    } else {
      map[movieLength] = true
    }
  }
  return false
}

console.log('Are there two movies for flight?', areThereTwoMoviesForFlight(10, [1, 8, 4, 3, 5]))
// => true (8 + 2 = 10)