/*
  Complexity
    O(n * m) time
    O(n) space, where n is the amount of money and m is the number of potential denominations
*/
const log = console.log

const changePossibilitiesBottomUp = (amount, denominations) => {
  const waysOfDoingNCents = new Array(amount + 1).fill(0)
  waysOfDoingNCents[0] = 1

  denominations.forEach(denomination => {
    for (let higherAmount = denomination; higherAmount <= amount; higherAmount++) {
      const remainder = higherAmount - denomination
      waysOfDoingNCents[higherAmount] += waysOfDoingNCents[remainder]
    }
  })

  return waysOfDoingNCents[amount]
}

console.log(changePossibilitiesBottomUp(5, [1, 3, 5]))
