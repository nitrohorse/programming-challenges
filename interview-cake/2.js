/*
  Complexity
    O(n) time
    O(n) space
    We make two passes through our input array, and the array we build always has the same length as the input array.
*/
const getProductsOfAllIntsExceptAtIndex = nums => {
  let products = []
  let product = 1
  for (let i = 0; i < nums.length; i++) {
    products[i] = product
    product *= nums[i]
  }

  product = 1
  for (let i = nums.length - 1; i >= 0; i--) {
    products[i] *= product
    product *= nums[i]
  }

  return products
}
console.log(getProductsOfAllIntsExceptAtIndex([3, 1, 2, 5, 6, 4]))
// => [240, 720, 360, 144, 120, 180]

console.log(getProductsOfAllIntsExceptAtIndex([1, 7, 3, 4]))
// => [84, 12, 28, 21]

console.log(getProductsOfAllIntsExceptAtIndex([0, 0, 0]))
// => [0, 0, 0]
