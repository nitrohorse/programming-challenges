/*
  Complexity
    O(1) time
    O(1) space
*/
const findRangeOverlap = (point1, length1, point2, length2) => {
  const start = Math.max(point1, point2)
  const end = Math.min(point1 + length1, point2 + length2)

  if (start >= end) {
    return {
      startPoint: null,
      overlapLength: null
    }
  }

  const overlapLength = end - start

  return {
    startPoint: start,
    overlapLength: overlapLength
  }
}

const findIntersection = (rectangle1, rectangle2) => {
  const xOverlap = findRangeOverlap(rectangle1.leftX, rectangle1.width, rectangle2.leftX, rectangle2.width)
  const yOverlap = findRangeOverlap(rectangle1.bottonY, rectangle1.height, rectangle2.bottonY, rectangle2.height)

  if (!xOverlap.overlapLength || !yOverlap.overlapLength) {
    return {
      leftX: null,
      bottomY: null,
      width: null,
      height: null
    }
  }

  return {
    leftX: xOverlap.startPoint,
    bottomY: yOverlap.startPoint,
    width: xOverlap.overlapLength,
    height: yOverlap.overlapLength
  }
}

const rectangle1 = {
  leftX: 1,
  bottonY: 1,
  width: 6,
  height: 3
}

const rectangle2 = {
  leftX: 5,
  bottonY: 2,
  width: 3,
  height: 6
}
console.log(findIntersection(rectangle1, rectangle2))
/*
  const intersection = {
    leftX: 5,
    bottonY: 2,
    width: 2,
    height: 2
  }
*/