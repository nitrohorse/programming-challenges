const isLetter = char => {
  return 'abcdefghijklmnopqrstuvwxyz'.includes(char.toLowerCase())
}

const addWordToMap = (map, word) => {
  word = word.toLowerCase()
  if (map.has(word)) {
    let count = map.get(word)
    map.set(word, count + 1)
  } else {
    map.set(word, 1)
  }
}

const wordCount = string => {
  // let words = []

  let map = new Map()

  let currentWordStartIndex = 0
  let currentWordLength = 0
  for (let i = 0; i < string.length; i++) {
    const char = string[i]

    if (isLetter(char)) {
      if (currentWordLength === 0) {
        currentWordStartIndex = i
      }
      currentWordLength += 1
    } else if (char === ' ') {
      let word = string.slice(currentWordStartIndex, currentWordStartIndex + currentWordLength)
      currentWordLength = 0
      addWordToMap(map, word)
    }
  }

  console.log(map)
}

let string1 = "We came, we saw, we conquered...then we ate Bill's (Mille-Feuille) cake."
let string2 = "Add milk and eggs, then add flour and sugar, don't ch'a know?"
console.log(wordCount(string1))
