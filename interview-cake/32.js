/*
  Complexity
    O(n) time
    O(n) space
*/
const sortScores = (unsortedScores, highestPossibleScore) => {
  let scoreCounts = Array(highestPossibleScore).fill(null)

  unsortedScores.forEach(score => {
    scoreCounts[score]++
  })

  let sortedScores = []
  for (let score = highestPossibleScore; score >= 0; score--) {
    let count = scoreCounts[score]

    while (count > 0) {
      sortedScores.push(score)
      count -= 1
    }
  }

  return sortedScores
}

let unsortedScores = [37, 89, 41, 65, 91, 53, 41, 41]
const HIGHEST_POSSIBLE_SCORE = 100

console.log(sortScores(unsortedScores, HIGHEST_POSSIBLE_SCORE))
// => [91, 89, 65, 53, 41, 37]