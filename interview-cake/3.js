/*
  Complexity
    O(n) time
    O(1) space
*/
const highestProduct = nums => {
  let highest = Math.max(nums[0], nums[1])
  let lowest = Math.min(nums[0], nums[1])
  let highestProductOf2 = nums[0] * nums[1]
  let lowestProductOf2 = nums[0] * nums[1]
  let highestProductOf3 = nums[0] * nums[1] * nums[2]

  for (let i = 2; i < nums.length; i++) {
    let current = nums[i]

    highestProductOf2 = Math.max(
      highestProductOf2,
      current * highest,
      current * lowest
    )

    lowestProductOf2 = Math.min(
      lowestProductOf2,
      current * highest,
      current * lowest
    )

    highestProductOf3 = Math.max(
      highestProductOf3,
      current * highestProductOf2,
      current * lowestProductOf2
    )

    highest = Math.max(highest, current)
    lowest = Math.max(lowest, current)
  }
  return highestProductOf3
}

console.log(highestProduct([5, 2, 9, 1, 7, 8]))
// => 504 (7 * 8 * 9)