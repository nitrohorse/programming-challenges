/*
  Complexity
    O(n log n) time
    O(n) space
*/
const mergeRanges = meetings => {
  const meetingsCopy = JSON.parse(JSON.stringify(meetings))
  // sort by start time
  const sorted = meetingsCopy.slice().sort((a, b) => {
    return a.startTime - b.startTime
  })

  const mergedRanges = new Array()
  for (let i = 1; i < sorted.length; i++) {
    const current = sorted[i - 1]
    const next = sorted[i]
    if (next.startTime <= current.endTime) {
      const merged = {
        startTime: current.startTime,
        endTime: next.endTime
      }
      mergedRanges.push(merged)
      i++
    } else {
      mergedRanges.push(current)
    }
  }
  return mergedRanges
}

const meetings = [
  { startTime: 10, endTime: 12 },
  { startTime: 4, endTime: 8 },
  { startTime: 0, endTime: 1 },
  { startTime: 3, endTime: 5 },
  { startTime: 9, endTime: 10 },
]
console.log(mergeRanges(meetings))
/* =>
[
  {startTime: 0, endTime: 1},
  {startTime: 3, endTime: 8},
  {startTime: 9, endTime: 12},
]
*/