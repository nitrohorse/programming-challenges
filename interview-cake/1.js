/*
  Complexity
    O(n) time
    O(1) space
    We only loop through the array once
*/
const getMaxProfit = stockPrices => {
  if (stockPrices.length < 2) {
    throw new Error('Getting a profit requires at least 2 prices')
  }

  let minPrice = stockPrices[0]
  let maxProfit = stockPrices[1] - stockPrices[0]
  
  for (let i = 1; i < stockPrices.length; i++) {
    let currentPrice = stockPrices[i]
    let potentialProfit = currentPrice - minPrice

    maxProfit = Math.max(maxProfit, potentialProfit)

    minPrice = Math.min(minPrice, currentPrice)
  }
  return maxProfit
}

console.log(getMaxProfit([10, 7, 5, 8, 11, 9]))
// => 6

console.log(getMaxProfit([10, 9, 8, 7]))
// => 0