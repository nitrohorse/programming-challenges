class LinkedListNode {
  constructor(value) {
    this.value = value
    this.next = null
  }
}

/*
  Complexity
    O(n) time
    O(1) space
*/
const hasCycle = head => {
  let oneStepRunner = head
  let twoStepRunner = head.next.next

  while (twoStepRunner && twoStepRunner.next) {
    if (twoStepRunner === oneStepRunner) {
      return true
    }
    oneStepRunner = oneStepRunner.next
    twoStepRunner = twoStepRunner.next.next
  }
  return false
}

const a = new LinkedListNode('A')
const b = new LinkedListNode('B')
const c = new LinkedListNode('C')
const d = new LinkedListNode('D')

a.next = b
b.next = c
c.next = d
d.next = a

// console.log('Linked List:', a)
// console.log('-----------------')

console.log('Has cycle?', hasCycle(a))
