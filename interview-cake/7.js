/*
  Complexity
    O(1) time
    O(1) space
*/

class TempTracker {
  constructor () {

    // for mean
    this.totalSum = 0
    this.numberOfReadings = 0
    this.mean = null

    // for mode
    this.mode = null
    this.occurances = new Array(111).fill(0)
    this.maxOccurances = null

    // for min and max
    this.minTemp = null
    this.maxTemp = null
  }

  insert (temp) {
    // for mode
    this.occurances[temp]++
    if (this.occurances[temp] > this.maxOccurances) {
      this.mode = temp
      this.maxOccurances = this.occurances[temp]
    }

    // for min and max
    if (!this.maxTemp || temp > this.maxTemp) {
      this.maxTemp = temp
    }
    if (!this.minTemp || temp < this.minTemp) {
      this.minTemp = temp
    }

    // for mean
    this.totalSum += temp
    this.numberOfReadings++
    this.mean = this.totalSum / this.numberOfReadings
  }

  getMax () {
    return this.maxTemp
  }

  getMin () {
    return this.minTemp
  }

  getMean () {
    return this.mean
  }

  getMode () {
    return 
  }
}

const tt = new TempTracker()
tt.insert(110)
tt.insert(75)
tt.insert(50)
tt.insert(75)
console.log('Max:', tt.getMax())
console.log('Min:', tt.getMin())
console.log('Mean:', tt.getMean())