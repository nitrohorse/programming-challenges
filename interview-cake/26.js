/*
  Complexity
    O(n) time
    O(1) space
*/
const reverseInPlace = s => {
  let stringArray = s.split('')
  const middle = Math.floor(stringArray.length / 2)

  for (let i = 0; i < middle; i++) {
    let temp = stringArray[i]
    stringArray[i] = stringArray[stringArray.length - 1 - i]
    stringArray[stringArray.length - 1 - i] = temp
  }

  return stringArray.join('')
}

let string = 'Reverse me!'
string = reverseInPlace(string)

console.log('Reversed:', string)