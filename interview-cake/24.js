class LinkedListNode {
  constructor(value) {
    this.value = value
    this.next = null
  }
}

/*
  Complexity
    O(n) time
    O(1) space
*/
const reverse = head => {
  if (!head || !head.next) {
    return head
  }

  let previous = null
  let current = head

  while (current) {
    let next = current.next
    current.next = previous
    previous = current
    current = next
  }
  return previous
}

const a = new LinkedListNode('A')
const b = new LinkedListNode('B')
const c = new LinkedListNode('C')

a.next = b
b.next = c

// console.log('Linked List:', a)
// console.log('-----------------')

console.log(reverse(a))

// console.log('Linked List reversed:', a)
// console.log('-----------------')