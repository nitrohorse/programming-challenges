const rand7 = () => {
  return Math.floor(Math.random() * 7) + 1;
}

/*
  Complexity
    O(infinity) time (we might keep re-rolling forever)
    O(1) space
*/
const rand5 = () => {
  let result = 7
  while (result > 5) {
    result = rand7()
  }
  return result
}

console.log(rand5())