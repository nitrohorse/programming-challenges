const findUniqueDeliveryId = deliveryIds => {
  // const map = new Map()

  let uniqueDeliveryId = 0

  deliveryIds.forEach(deliveryId => {
    uniqueDeliveryId ^= deliveryId
    //   if (!map.has(deliveryId)) {
    //     map.set(deliveryId, 1)
    //   } else {
    //     map.delete(deliveryId)
    //   }
    // })
    // for (let [deliveryId, count] of map) {
    //   if (count === 1) {
    //     return deliveryId
    //   }
  })
  return uniqueDeliveryId
}

console.log('Unique delivery ID:', findUniqueDeliveryId([4, 3, 2, 9, 2, 3, 4, 9, 5, 7, 5]))
// => 7