/*
  Complexity
    O(m) time for m calls
    O(n) space
*/
class QueueWithTwoStacks {
  constructor() {
    this.inStack = []
    this.outStack = []
  }

  enqueue(value) {
    while (this.outStack.length) {
      this.inStack.push(this.outStack.pop())
    }
    this.inStack.push(value)
  }

  dequeue() {
    while (this.inStack.length) {
      this.outStack.push(this.inStack.pop())
    }
    if (!this.outStack.length) {
      return undefined
    }
    return this.outStack.pop()
  }
}

const q = new QueueWithTwoStacks()
q.enqueue('a')
q.enqueue('b')
q.enqueue('c')
console.log('q.inStack:', q.inStack)
console.log('q.outStack:', q.outStack)
console.log('dequeued:', q.dequeue())
console.log('dequeued:', q.dequeue())
q.enqueue('d')
q.enqueue('e')
console.log('q.inStack:', q.inStack)
console.log('q.outStack:', q.outStack)
console.log('dequeued:', q.dequeue())
console.log('q.inStack:', q.inStack)
console.log('q.outStack:', q.outStack)
console.log('dequeued:', q.dequeue())
console.log('dequeued:', q.dequeue())
console.log('dequeued:', q.dequeue())