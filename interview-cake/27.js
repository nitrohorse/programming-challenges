const reverseCharacters = (message, leftIndex, rightIndex) => {
  while (leftIndex < rightIndex) {
    let temp = message[leftIndex]
    message[leftIndex] = message[rightIndex]
    message[rightIndex] = temp
    leftIndex++
    rightIndex--
  }
}

/*
  Complexity
    O(n) time
    O(1) space
*/
const reverseWords = message => {
  // reverseCharacters(message, 0, message.length - 1)

  // let currentWordStartIndex = 0

  // for (let i = 0; i <= message.length; i++) {
  //   if (i === message.length || message[i] === ' ') {
  //     reverseCharacters(message, currentWordStartIndex, i - 1)
  //     currentWordStartIndex = i + 1
  //   }
  // }

  // return

  const wordArray = message.split(' ')

  for (let i = 0; i < Math.floor(wordArray.length / 2); i++) {
    const temp = wordArray[i]

    wordArray[i] = wordArray[wordArray.length - 1 - i]
    wordArray[wordArray.length - 1 - i] = temp
  }

  return wordArray.join(' ')
}

// const message = ['c', 'a', 'k', 'e', ' ',
//   'p', 'o', 'u', 'n', 'd', ' ',
//   's', 't', 'e', 'a', 'l']

const message = 'cake pound steal'
console.log(reverseWords(message))
// reverseWords(message)

// console.log(message.join(''))
// => steal pound cake
