const getRandom = (floor, ceiling) => {
  return Math.floor(Math.random() * (ceiling - floor + 1)) + floor;
}

/*
  Complexity
    O(n) time
    O(1) space
*/
const shuffle = array => {
  if (array.length <= 1) {
    return
  }

  for (let i = 0; i < array.length - 1; i++) {
    const randomIndex = getRandom(i, array.length - 1)

    if (randomIndex !== i) {
      const valueAtIndex = array[i]
      array[i] = array[randomIndex]
      array[randomIndex] = valueAtIndex
    }
  }
  return array
}

const array = [1, 2, 3, 4, 5]
console.log(shuffle(array))