class LinkedListNode {
  constructor(value) {
    this.value = value
    this.next = null
  }
}

/*
  Complexity
    O(n) time
    O(1) space
*/
const getKthToLastNode = (k, head) => {
  let kAway = head
  let current = head

  // get k away
  while (k > 0 && current) {
    current = current.next
    k--
  }
  if (k !== 0) {
    throw new Error('k is larger than the length of the linked list')
  }

  while (current) {
    kAway = kAway.next
    current = current.next
  }

  return kAway.value
}

const a = new LinkedListNode('A')
const b = new LinkedListNode('B')
const c = new LinkedListNode('C')
const d = new LinkedListNode('D')
const e = new LinkedListNode('E')
const f = new LinkedListNode('F')

a.next = b
b.next = c
c.next = d
d.next = e
e.next = f

console.log('2nd to last node:', getKthToLastNode(2, a))
// => E