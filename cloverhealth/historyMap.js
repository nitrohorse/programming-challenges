/*
  design a Key/Value data structure that preserves the history of values for any key.
  Must support at least two public methods:
    Set: takes a string key, and any value
    Get: takes a string key, and an optional version identifier.

 */
const log = console.log

class HistoryMap {
  constructor() {
    this.historyMap = {}
  }

  set(key, value) {
    if (this.historyMap[key]) { // contains
      this.historyMap[key].unshift(value)
    } else {
      this.historyMap[key] = [value]
    }
  }

  // get most recent value for key
  // if no id provided

  // id => 3 => 3rd most recent
  get(key, id /*optional*/) {
    if (this.historyMap[key]) {
      const values = this.historyMap[key]

      if (id === undefined) {
        return values[0]
      }

      let counter = 1

      for (let i = 0; i < values.length; i++) {
        const value = values[i]

        if (counter === id) {
          return value
        }
        counter++
      }
    } else {
      return -1
    }
  }
}

const hm = new HistoryMap()
hm.set('A', 'Andy')
hm.set('banana', '911')
hm.set('banana', '2092696491')
hm.set('banana', 'second value')

log(hm.get('banana')) // => most recent
log(hm.get('banana', 3)) // => 911



// log(hm.get('banana', 10))
// log(hm.get('tomato'))
// log(hm.historyMap['banana'][0])
log(hm)


// set('banana', 'first value')
// set('banana', 'second value')
// get('banana') -> 'second value'