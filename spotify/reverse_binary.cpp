// Andreas Basurto
// Problem ID: reversebinary
// https://www.spotify.com/us/jobs/tech/reversed-binary/

#include <iostream>
#include <string>
#include <algorithm>

void dec_to_bin(int num, std::string& bin);
int bin_to_dec(const std::string& bin);

int main()
{
    int num;
    std::cin >> num;
    std::string bin = "";
    dec_to_bin(num, bin);
    std::cout << bin_to_dec(bin) << std::endl;
    return 0;
}

void dec_to_bin(int num, std::string& bin)
{
    do
    {
        if ((num & 1) == 0)
            bin += "0";
        else
            bin += "1";
        num >>= 1;
    } while (num);
}

int bin_to_dec(const std::string& bin)
{
    int i, dec = 0;
    for(i = 0; i < bin.length() - 1; ++i)
        dec = 2 * (dec + (bin[i] - '0'));
    dec += (bin[i] - '0');
    return dec;
}