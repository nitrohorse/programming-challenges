// Andreas Basurto
// Problem ID: zipfsong
// https://www.spotify.com/us/jobs/tech/zipfsong/

#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <map>

using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::pair;
using std::map;
using std::string;

void print_vec_pairs(const vector<pair<long long, string> >& songs)
{
    for (int i = 0; i < songs.size(); ++i)
    {
        cout << songs[i].first << " => " << songs[i].second << endl;
    }
}

void print_map(const map<double, string>& quality_of_songs)
{
    for (map<double, string>::const_iterator it = quality_of_songs.begin(); it != quality_of_songs.end(); ++it)
    {
        cout << it->first << " => " << it->second << endl;
    }
}

void calculate_quality_of_songs(map<double, string>& quality_of_songs, const vector<pair<long long, string> >& songs, const long long& highest_freq)
{
    double quality = 0;
    for (int i = 0; i < songs.size(); ++i)
    {
        quality = double(songs[i].first/double(highest_freq/(i+1)));
        quality_of_songs[quality] = songs[i].second;
    }
}

void calculate_highest_freq(const vector<pair<long long, string> >& songs, long long& highest_freq)
{
    highest_freq = 0;
    for (int i = 0; i < songs.size(); ++i)
    {
        if (songs[i].first > highest_freq)
        {
            highest_freq = songs[i].first;
        }
    }
}

void print_highest_qualities(const map<double, string>& quality_of_songs, const unsigned int& select_songs)
{
    map<double, string>::const_reverse_iterator rit = quality_of_songs.rbegin();
    for (int i = 0; i < select_songs; ++i)
    {
        cout << rit->second << endl;
        ++rit;
    }
}

int main()
{
    unsigned int num_songs = 0; // 1 <= n <= 50,000
    unsigned int select_songs = 0; // 1 <= m <= n
    long long freq_of_plays = 0;
    long long highest_freq = 0;
    string song_name = "";
    vector<pair<long long, string> > songs(num_songs, std::make_pair(0, ""));
    map<double, string> quality_of_songs;

    cin >> num_songs >> select_songs;

    while (num_songs--)
    {
        cin >> freq_of_plays >> song_name;
        songs.push_back(std::make_pair(freq_of_plays, song_name));
    }

    calculate_highest_freq(songs, highest_freq);
    // cout << "Highest freq: " << highest_freq << endl << endl;

    // print_vec_pairs(songs);
    // cout << endl;

    calculate_quality_of_songs(quality_of_songs, songs, highest_freq);

    // print_map(quality_of_songs);
    // cout << endl;

    print_highest_qualities(quality_of_songs, select_songs);

    return 0;
}