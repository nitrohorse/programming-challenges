import * as React from 'react'
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'

import CampFeature from './CampFeature'

describe ('CampFeature', () => {
  const campFeature = {
    'title': 'Camp Feature X',
    'presence': true,
    'subfeatures': [
      {
        'title': 'Camp SubFeature Y',
        'presence': true,
        'subfeatures': []
      },
    ]
  }

  const component = shallow(
    <CampFeature campFeature={campFeature} campSubFeatures={campFeature.subfeatures} />
  )

  it ('renders and matches our snapshot', () => {
    const component = renderer.create(
      <CampFeature campFeature={campFeature} campSubFeatures={campFeature.subfeatures} />
    )
    const tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it ('contains the supplied camp feature title', () => {
    expect(component.text()).toContain(campFeature.title)
  })
})