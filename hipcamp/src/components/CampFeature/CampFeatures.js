// @flow
import * as React from 'react'

import './CampFeatures.css'

import CampFeature from './CampFeature'
import CampFeaturesService from '../../services/CampFeaturesService'
import CampFeatureInterface from './CampFeatureInterface'

type Props = {
  campFeature: CampFeatureInterface
}

type State = {
  campFeatures: Array<CampFeatureInterface>
}

class CampFeatures extends React.Component<Props, State> {
  constructor (props: Props): void {
    super(props)

    this.state = {
      campFeatures: []
    }
  }

  componentDidMount (): void {
    this.setState(() => ({
      campFeatures: CampFeaturesService.getCampFeatures()
    }))
  }

  render (): React$Element<string> {
    const campFeatures = this.state.campFeatures.map(campFeature => {
      return (
        <CampFeature
          key={campFeature.title}
          campFeature={campFeature}
          campSubFeatures={campFeature.subfeatures}
        />
      )
    })

    return (
      <ul className='top-level-camp-features'>
        {campFeatures}
      </ul>
    )
  }
}

export default CampFeatures