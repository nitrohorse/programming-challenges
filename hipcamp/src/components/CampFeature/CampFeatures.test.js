import * as React from 'react'
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'

import CampFeatures from './CampFeatures'
import CampFeature from './CampFeature'

describe ('CampFeatures', () => {
  const component = shallow(
    <CampFeatures />
  )

  it ('renders and matches our snapshot', () => {
    const component = renderer.create(
      <CampFeatures />
    )
    const tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it ('contains four CampFeature subcomponents', () => {
    expect(component.find(CampFeature)).toHaveLength(4)
  })

  it ('contains the same number of CampFeature subcomponents as camp features '
    + 'in the state', () => {
    const numOfCampFeatures = component.state('campFeatures').length
    const numOfCampFeatureComponents = component.find(CampFeature).length
    expect(numOfCampFeatures).toEqual(numOfCampFeatureComponents)
  })
})