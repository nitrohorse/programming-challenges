// @flow
import * as React from 'react'
import ReactShow from 'react-show'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'

import './CampFeature.css'
import CampFeatureInterface from './CampFeatureInterface'

type Props = {
  campFeature: CampFeatureInterface,
  campSubFeatures: Array<CampFeatureInterface>
}

type State = {
  showCampSubFeatures: boolean
}

class CampFeature extends React.Component<Props, State> {
  constructor (props: Props): void {
    super(props)

    this.state = {
      showCampSubFeatures: false
    }
  }

  handleCollapse (event: Event): void {
    this.setState({
      showCampSubFeatures: !this.state.showCampSubFeatures
    })
    event.stopPropagation()
  }

  determineIconName (campFeatureTitle: string): string {
    let iconName = ''
    switch (campFeatureTitle) {
      case 'Toilet':
        iconName = 'archive'
        break
      case 'Pets allowed':
        iconName = 'paw'
        break
      case 'Shower':
      case 'Outdoor shower':
        iconName = 'shower'
        break
      case 'Trash':
      case 'Trash bin':
        iconName = 'trash'
        break
      case 'Recycling bin':
        iconName = 'recycle'
        break
      case 'Compost bin':
        iconName = 'trash-alt'
        break
      case 'Pack in, pack out':
        iconName = 'shopping-bag'
        break
      default:
        break
    }
    return iconName
  }

  render (): React$Element<string> {
    let campSubFeatures = null;

    if (this.props.campSubFeatures) {
      campSubFeatures = this.props.campSubFeatures.map(campSubFeature => {
        return (
          <CampFeature
            key={campSubFeature.title}
            campFeature={campSubFeature}
            campSubFeatures={campSubFeature.subfeatures}
          />
        )
      })
    }

    const doesCampSubFeaturesExist = campSubFeatures && campSubFeatures.length > 0

    return (
      <li
        key={this.props.campFeature.title}
        className={doesCampSubFeaturesExist ? 'has-camp-sub-features' : 'no-camp-sub-features'}
        onClick={this.handleCollapse.bind(this)}>
        <span className={this.props.campFeature.presence ? 'available' : 'not-available'}>
          <FontAwesomeIcon icon={this.determineIconName(this.props.campFeature.title)} /> {this.props.campFeature.title}
        </span>
        <ReactShow show={this.state.showCampSubFeatures}>
          {doesCampSubFeaturesExist ? <ul>{campSubFeatures}</ul> : ''}
        </ReactShow>
      </li>
    )
  }
}

export default CampFeature