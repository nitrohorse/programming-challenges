export interface CampFeatureInterface {
  title: string,
  presence: boolean,
  subfeatures: Array<CampFeatureInterface>
}
