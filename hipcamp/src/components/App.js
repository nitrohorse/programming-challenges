import * as React from 'react'
import fontawesome from '@fortawesome/fontawesome'
import solid from '@fortawesome/fontawesome-free-solid'

import './App.css'

import CampFeatures from './CampFeature/CampFeatures'

class App extends React.Component {
  componentDidMount () {
    fontawesome.library.add(solid)
  }

  render () {
    return (
      <section className='camp-features-section'>
        <h3>Camp Features</h3>
        <CampFeatures/>
      </section>
    )
  }
}

export default App
