// Write a function that accepts a string and returns whether or not
// the string is a valid ipv4 address. Your function should avoid
// unnecessary computation where possible.

// Valid IPv4 addresses have 4 octets represented as a decimal with range 0-255, delimited by periods.

// Examples:
// <0-255>.<0-255>.<0-255>.<0-255>
// 127.0.0.1 

// 2nd function
// string -> 0000
// 

const isWithinRange = string => {
  for (let i = 0; i < string.length; i++) {
    const char = string[i]
    
    if (!'0123456789'.includes(char)) {
      return false
    }
  }
  
  return (Number(string) >= 0 && Number(string) <= 255)
}

const isValidIPv4Address = string => {
  let octets = string.split('.')
  
  if (octets.length < 4 || octets.length > 4) {
    return false
  }
  
  for (let i = 0; i < octets.length; i++) {
    const octet = octets[i]
    
    if (octet.length > 3) {
      return false
    }
    if (!isWithinRange(octet)) {
      return false
    }
  }
  
  return true
}

console.log(isValidIPv4Address('127.0.0.1')) // true
console.log(isValidIPv4Address('127.0.0.a')) // false
console.log(isValidIPv4Address('127.0.0')) // false
console.log(isValidIPv4Address('127.0.0.1.')) // false
console.log(isValidIPv4Address('hello world')) // false
console.log(isValidIPv4Address('hello world. hello world. hello world. hello world.')) // false

// write a new function that accepts a string of integers and
// returns whether a valid IPv4 address can be created using
// exactly and only those integer

// string is guaranteed to only contain digits 0-9

// Example: '0000'    | 0.0.0.0      | true
// Example: '8675309' | 86.75.30.9   | true
// Example: '777'     | 7.7.7        | false

/*
  validate
    length > 4
    digits within each octet are within 0-255
    8675309
    
    99.99.99.9999
    
  check if length of digit string is > 4
    if not, return false
  iterate through each character of the digit string
    create new octet
      absorb a digit into octet if
        the octet's length is > 1 && < 3
        the octet will be in range of 0 and 255
   
*/

const canCreateIPv4Address = digitString => {
  if (digitString.length < 4) {
    return false
  }
  let ipv4Address = ''
  
  for (let i = 0; i < digitString.length; i++) {
    let octet = ''
    
    if (octet.length < 3) {
      
    }
  }
}







