/* 
Your previous Ruby content is preserved below:

# Create a calculator that takes integers separated by the basic mathematical operators (+,-,*,/) and returns the result.

# You can use Ruby's mathamatical methods(+, -, *, /).
# The priority is on working code, not brevity.

# 7 + 2 - 3 / 4 * 5 = 5.25

math_characters = '7 + 2 - 3 / 4 * 5'

# magic

math_result = 5.25

puts "Result of #{math_characters} is: #{math_result}"
 */

const log = console.log

/*
  Create a calculator
    Takes in ints separated by +, -, /, *
    Use standard order of ops to compute result
    
  calc = new Calc()
  equation = '8 + 1 * 2'
  calc.compute(equation) => 18
*/
class Calculator {
  constructor() {
    
  }
  
  compute(equation) {
    const equationArray = equation.split(' ')
    
    // 10 / 2 + 1    
    let index = 1
    while (index < equationArray.length) {
      const operator = equationArray[index]

      if (operator === '/' || operator === '*') {
        // combine neighbors
        // 10 / 2
        const leftChar = equationArray[index - 1]
        const rightChar = equationArray[index + 1]

        let result = 0
        if (operator === '/') {
          result = Number(leftChar) / Number(rightChar)
        } else {
          result = Number(leftChar) * Number(rightChar)
        }

        // 1 + 10 / 2 + 3
        // 1 + 5 / 2 + 3          
        equationArray[index - 1] = result

        // 1 + 5 + 3
        equationArray.splice(index, 2)
      }
      index += 2
    }
    
    index = 1
    while (index < equationArray.length && equationArray.length !== 1) {
      const operator = equationArray[index]
      let result = 0
      const leftChar = equationArray[index - 1]
      const rightChar = equationArray[index + 1]
      if (operator === '+') {
        result = Number(leftChar) + Number(rightChar)
      } else {
        result = Number(leftChar) - Number(rightChar)
      }

      equationArray[index - 1] = result
      equationArray.splice(index, 2)
    }

    return Number(equationArray[0])
  }
}

const c = new Calculator()
const equation = '1 + 5 + 3' // => 9
// '7 + 2 - 3 / 4 * 5'
log(c.compute(equation))
// => 18