const printMatrixAsSpiral = matrix => {
  const n = matrix.length
  let max = n - 1
  let min = 0
  let counter = 0

  while (counter <= Math.floor(n / 2)) {
    for (let i = min; i < max; i++) {
      console.log(matrix[min][i])
    }
    for (let i = min; i < max; i++) {
      console.log(matrix[i][max])
    }
    for (let i = max; i >= min; i--) {
      console.log(matrix[max][i])
    }
    for (let i = max; i > min; i--) {
      console.log(matrix[i][min])
    }
    min++
    max--
    counter++
  }
}

const matrix = [
  [1, 2, 3, 4, 5],
  [16, 17, 18, 19, 6],
  [15, 24, 25, 20, 7],
  [14, 23, 22, 21, 8],
  [13, 12, 11, 10, 9]
]
console.log(printMatrixAsSpiral(matrix))