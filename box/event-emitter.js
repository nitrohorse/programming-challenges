/*
  instantiate EventEmitter
    - register(eventName, function)
    - broadcast(eventName)
      - call eventName's callback function
*/

const log = console.log

class EventEmitter {
  constructor() {
    // key/value store for events
    // key - string - eventName
    // value - array - callbacks
    this.events = new Map()
  }

  register(eventName, callback, isOnce) {
    if (!this.events.has(eventName)) {
      const callbacks = new Array()

      callbacks.push({
        callback,
        isOnce
      })

      this.events.set(eventName, callbacks)
    } else {
      const callbacks = this.events.get(eventName)

      callbacks.push({
        callback,
        isOnce
      })

      this.events.set(eventName, callbacks)
    }
    //log(this.events)    
  }

  broadcast(eventName, ...args) {
    if (this.events.get(eventName)) {
      const callbacks = this.events.get(eventName)

      callbacks.forEach(callbackObj => {
        callbackObj.callback(...args)

        if (callbackObj.isOnce) {
          // remove from callbacks array
          callbacks.shift()
        }
      })
    } else {
      // do nothing for now
    }
  }

  registerOnce(eventName, callback) {
    this.register(eventName, callback, isOnce)
  }
}



// example code
var e = new EventEmitter();

e.registerOnce('foo', function () {
  console.log('Fired once');
});

e.register('foo', function () {
  console.log('Always fired');
});

e.broadcast('foo');
// Fired once
// Always fired
e.broadcast('foo');
// Always fired
