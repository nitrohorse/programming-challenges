/*
  Write a function that gets a integer and returns true if the integer is prime.
*/

const isPrime = num => {
  if (num % 2 === 0) {
    return false
  }

  for (let i = 3; i <= Math.ceil(Math.sqrt(num)); i += 2) {
    if (num % i === 0) {
      return false
    }
  }

  return true
}

console.log(isPrime(9))
console.log(isPrime(7))
console.log(isPrime(17))
console.log(isPrime(99))