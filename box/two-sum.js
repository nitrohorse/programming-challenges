/*
  Given an array of integers, find all the sets of two integers that add up to a target number.
*/
const log = console.log

const twoSum = (array, target) => {
  const set = new Set()
  const twoSumSet = new Map()

  array.forEach(integer => {
    if (!set.has(integer)) {
      set.add(integer)
    }
  })

  array.forEach(integer => {
    const diff = target - integer
    if (set.has(diff) && !twoSumSet.has(integer, diff) && !twoSumSet.has(diff, integer)) {
      twoSumSet.set(integer, diff)
    }
  })

  return twoSumSet
}

const array = [3, 8, 9, 10, 3, 7, 4, 2, 1, 5, 6]
const target = 11
console.log(twoSum(array, target))