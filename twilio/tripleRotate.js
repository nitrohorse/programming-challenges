/*
  Twilio
  On a whiteboard, implement a function called tripleRotate, which "rotates" all subsequent sets of 3 elements in an array, when the algorithm for one rotation is as follows:

  Given the triplet ['a', 'b', 'c'], the 1st element ('a') is moved to index 1, the 2nd element ('b') is moved to index 2 and the 3rd element ('c') is moved to index 0. Thus, one rotation of the given triplet results in the new array ['c', 'a', 'b'].
*/

const rotate = array => {
  // swap 1st and 3rd elements
  let temp = array[0]
  array[0] = array[2]
  array[2] = temp

  // swap 2nd and 3rd elements
  temp = array[1]
  array[1] = array[2]
  array[2] = temp

  return array
}

const tripleRotate = array => {
  let rotatedArray = []
  let setOfThree = []
  for (let index = 0; index < array.length; index++) {
    setOfThree.push(array[index])

    if (setOfThree.length === 3) {
      setOfThree = rotate(setOfThree)
      rotatedArray = rotatedArray.concat(setOfThree)
      setOfThree = []
    }
  }

  if (array.length % 3 !== 0) {
    const diff = array.length - rotatedArray.length
    rotatedArray = rotatedArray.concat(array.slice(-diff))
  }

  return rotatedArray
}

console.log(tripleRotate(['a', 'b']))
// => [ 'a', 'b' ]

console.log(tripleRotate(['a', 'b', 'c']))
// => [ 'c', 'a', 'b' ]

console.log(tripleRotate(['a', 'b', 'c', 'd', 'e', 'f']))
// => [ 'c', 'a', 'b', 'f', 'd', 'e' ]

console.log(tripleRotate(['a', 'b', 'c', 'd', 'e', 'f', 'g']))
// => [ 'c', 'a', 'b', 'f', 'd', 'e', 'g' ]

console.log(tripleRotate(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']))
// => [ 'c', 'a', 'b', 'f', 'd', 'e', 'g', 'h' ]