// http://redd.it/1hvh6u
// Challenge #132 [Easy] Greatest Common Divisor

#include <iostream>
#include <cmath>
using namespace std;

int gcd(int x, int y) {
    while (x != y) {
        if (x > y) x -= y;
        else y -= x;
    }
    return abs(x);
}

// Iterative Euclid algorithm
int gcd_iter(int x, int y) {
    int t;
    while (y) {
        t = x;
        x = y;
        y = t % y;
    }
    return abs(x);
}

// Recursive Euclid algorithm
int gcd_rec(int x, int y) {
    return (y == 0) ? x : gcd_rec(y, x % y);  
}

int main() {
    int x, y;
    cout << "Enter first number: "; cin >> x;
    cout << "Enter second number:"; cin >> y;
    cout << "GCD (iterative): " << gcd(x, y) << endl;
    cout << "GCD (iterative Euclid): " << gcd_iter(x,y) << endl;
    cout << "GCD (recursive Euclid): " << gcd_rec(x, y) << endl;
    return 0;
}