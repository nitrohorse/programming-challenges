// http://redd.it/1f7qp5
// Challenge #127 [Easy] McCarthy 91 Function

#include <iostream>

int M(long num)
{
    if (num > 100) {
        std::cout << "M(" << num - 10 << ") since " 
                  << num << " > 100" << std::endl;
        return num - 10;
    } else {
        std::cout << "M(M(" << num + 11 << ")) since " 
                  << num << " <= 100" << std::endl;
        return M(M(num + 11));  
    }
}

int main() 
{
    long num;
    std::cin >> num;
    std::cout << M(num) << std::endl;
    return 0;
}