// http://redd.it/1e97ob
// Challenge #125 [Easy] Word Analytics

#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <cctype>
#include <vector>
#include <algorithm>

using namespace std;

template<typename A, typename B>
pair<B,A> flip_pair(const pair<A,B> &p)
{
    return pair<B,A>(p.second, p.first);
}

template<typename A, typename B>
multimap<B,A> flip_map(const map<A,B> src)
{
    multimap<B,A> dst;
    transform(src.begin(), src.end(), 
              inserter(dst, dst.begin()), 
              flip_pair<A,B>);
    return dst;
}

int main(int num_args, char** arg_strings) {
    if (num_args != 2) {
        cerr << "Error: No path to txt file" << endl;
        return 1;
    }
    ifstream ifs(arg_strings[1]);
    map<string, int> word_map;
    map<char, int> alpha_map;
    map<char, int> punct_map;
    string w="";
    char c='a';
    string line="";

    while (getline(ifs, line)) 
    {
        for (int i = 0; i < line.length(); ++i)
        {
            if (isspace(line[i]) || (ispunct(line[i]) && line[i] != '\'')) 
            {
                if (!w.empty())
                {
                    word_map[w]++;            
                    w="";
                }
                punct_map[line[i]]++;
            }
            else if (isalpha(tolower(line[i]))) 
            {
                w+=line[i];
                alpha_map[line[i]]++; 
            }
            else if (line[i] == '\'') 
            {   
                w+=line[i]; 
                punct_map[line[i]]++;
            } 
            else punct_map[line[i]]++;
        }
    }
    ifs.close();

    int total_words=0;
    int total_letters=0;
    int total_symbols=0;

    multimap<int,string> f_word_map = flip_map(word_map);
    multimap<int,string>::reverse_iterator rit1=f_word_map.rbegin();
    vector<string> top_words;
    
    multimap<int,char> f_alpha_map = flip_map(alpha_map);
    multimap<int,char>::reverse_iterator rit2=f_alpha_map.rbegin();
    vector<char> top_letters;

    int i=0;
    while (rit1->first > 1 && i < 3) 
    {
        top_words.push_back(rit1->second); 
        ++rit1;
        ++i;
    } 
    i=0;
    while (rit2->first > 1 && i < 3)
    {
        top_letters.push_back(rit2->second); 
        ++rit2;
        ++i;
    } 

    for (map<string, int>::iterator it=word_map.begin();it!=word_map.end();++it) {
        total_words+=it->second;
    }

    for (map<char, int>::iterator it=alpha_map.begin();it!=alpha_map.end();++it) {
        total_letters+=it->second;
    }

    for (map<char, int>::iterator it=punct_map.begin();it!=punct_map.end();++it) {
        total_symbols+=it->second;
    }

    cout << total_words << " words" << endl
         << total_letters << " letters" << endl
         << total_symbols << " symbols (inc. spaces)" << endl;
         switch (top_words.size()) 
         {
            case 3:
                cout << "Top three most common words: " + top_words[0] + ", " + top_words[1] + ", and " + top_words[2] << endl;
                break;
            case 2:
                cout << "Top two most common words: " + top_words[0] + " and " + top_words[1] << endl;
                break;
            case 1:
                cout << "Most common word: " + top_words[0] << endl;
                break;
            default:
                break;
         }
         // switch (top_letters.size()) 
         // {
         //    case 3:
         //        cout << "Top three most common letters: " + top_letters[0] + ", " + top_letters[1] + ", and " + top_letters[2] << endl;
         //        break;
         //    case 2:
         //        cout << "Top two most common letters: " + top_letters[0] + " and " + top_letters[1] << endl;
         //        break;
         //    case 1:
         //        cout << "Most common letter: " + top_letters[0] << endl;
         //        break;
         //    default:
         //        break;
         // }
         cout << " is the most common first word of all paragraphs" << endl
         << "Words only used once: _, _, ..." << endl
         << "Letters not used in the document: _, _, ..." << endl;

    return 0;
}