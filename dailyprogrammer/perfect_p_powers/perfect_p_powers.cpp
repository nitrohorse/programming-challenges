#include <iostream>
#include <cmath>

using namespace std;

int main() {
    unsigned long num; // 4294967295 (232-1) limit
    cin >> num;
    cout << pow(num, 2.0) << endl;
    return 0;
}