// http://redd.it/1epasu
// Challenge #126 [Easy] Real-World Merge Sort

// inplace_merge example
#include <iostream>     // std::cout
#include <algorithm>    // std::inplace_merge, std::sort, std::copy
#include <vector>       // std::vector

int main () {
  int first[] = {692,1,32};
  int second[] = {14,15,123,2431};
  std::vector<int> v(7);
  std::vector<int>::iterator it;

  it=std::copy (first, first+3, v.begin());
     std::copy (second,second+4,it);

  std::inplace_merge (v.begin(),v.begin()+4,v.end());

  std::cout << "The resulting vector contains:";
  for (it=v.begin(); it!=v.end(); ++it)
    std::cout << ' ' << *it;
  std::cout << '\n';

  return 0;
}