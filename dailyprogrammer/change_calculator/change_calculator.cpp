// http://redd.it/17f3y2
// Challenge #119 [Easy] Change Calculator

#include <iostream>
#include <cmath>

using namespace std;

double roundToNearestHundredth(double number)
{
    return (floor(number * 100 + 0.5) / 100);
}

int main() 
{
    double amount;
    cin >> amount;
    amount = roundToNearestHundredth(amount);
    int amnt = static_cast<int>(amount * 100);
    cout << "Quarters: " << amnt/25 << endl;
    amnt %= 25;
    cout << "Dimes: " << amnt/10 << endl;
    amnt %= 10;
    cout << "Nickels: " << amnt/5 << endl;
    amnt %= 5;
    cout << "Pennies: " << amnt/1 << endl;
    return 0;
}