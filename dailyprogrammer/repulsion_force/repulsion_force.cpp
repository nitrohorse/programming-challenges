// http://redd.it/1ml669
// Challenge #138 [Easy] Repulsion-Force

#include <iostream>
#include <stdio.h>
#include <cmath>
using namespace std;

float getRepulsionForce(const float& particle_1_mass,
                        const float& particle_1_x,
                        const float& particle_1_y,
                        const float& particle_2_mass,
                        const float& particle_2_x,
                        const float& particle_2_y)
{
    float deltaX = particle_1_x - particle_2_x;
    float deltaY = particle_1_y - particle_2_y;
    float distance = sqrt(pow(deltaX, 2.0) + pow(deltaY, 2.0));
    return ((particle_1_mass * particle_2_mass) / pow(distance, 2.0));
}

int main() 
{
    float particle_1_mass = 0;
    float particle_1_x    = 0;
    float particle_1_y    = 0;
    float particle_2_mass = 0;
    float particle_2_x    = 0;
    float particle_2_y    = 0;

    cin >> particle_1_mass
        >> particle_1_x
        >> particle_1_y
        >> particle_2_mass
        >> particle_2_x
        >> particle_2_y;
        
    printf("%.4f\n", getRepulsionForce(particle_1_mass,
                                       particle_1_x,
                                       particle_1_y,
                                       particle_2_mass,
                                       particle_2_x,
                                       particle_2_y));
    return 0;
}