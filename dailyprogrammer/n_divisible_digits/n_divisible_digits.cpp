// http://redd.it/1jtryq
// Challenge #134 [Easy] N-Divisible Digits 

#include <iostream>
#include <fstream>
#include <cmath>

int main(){
    std::ifstream ifs("n_divisible_digits_test1.txt");
    int num1=0;
    int num2=0;
    int max_int=1;
    ifs>>num1>>num2;
    max_int = (int)pow(10.0,(double)num1)-1;
    for(;max_int>0;--max_int){
        if(max_int%num2==0){
            std::cout<<max_int<<std::endl; 
            break;
        }
    }
    return 0;
}