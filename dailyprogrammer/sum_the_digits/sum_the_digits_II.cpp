// http://redd.it/1fnutb
// Challenge #128 [Easy] Sum-the-Digits, Part II

#include <iostream>
#include <sstream>

using namespace std;

string sumTheDigits(string num_str);

int main() {
    string s="";
    cin >> s;
    sumTheDigits(s);
    return 0;
}

string sumTheDigits(string num_str) {
    cout << num_str << endl;
    if (num_str.length() == 1) 
        return num_str;
    int sum = 0;
    stringstream ss("");
    for (int i = 0; i < num_str.length(); ++i) 
        sum += num_str[i] - '0';
    ss << sum;
    return sumTheDigits(ss.str());
}