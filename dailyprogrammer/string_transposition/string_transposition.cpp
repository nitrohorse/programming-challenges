// http://redd.it/1m1jam
// Challenge #137 [Easy] String Transposition

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int main() {
    std::string input="";
    int width=0;
    int height=0;
    std::ifstream ifs("test.txt");    
    ifs>>width;
    std::vector<std::string> v_words(width);
    ifs.ignore(1,'\n');
    for(int i=0; i<width;++i){
        getline(ifs,input);
        if (input.length() > height) height = input.length();
        v_words[i]=input;         
    }
    for (int i=0;i<height;++i){
        for(int j=0;j<width;++j) 
            if (i < v_words[j].length()) std::cout<<v_words[j][i]; 
            else std::cout<<' ';
        std::cout<<std::endl;
    }   
    return 0;
}