class dnaTranscriber {
	constructor() {
		this.map = {
			'G': 'C',
			'C': 'G',
			'T': 'A',
			'A': 'U'
		};
	}

	toRna(dnaSequence) {
		let rna = '';
		dnaSequence.split('').forEach(dna => {
			rna += this.map[dna];
		});
		
		if (rna.length === dnaSequence.length) {
			return rna;
		} else {
			throw Error('Invalid input');
		}
	}
}

module.exports = dnaTranscriber;