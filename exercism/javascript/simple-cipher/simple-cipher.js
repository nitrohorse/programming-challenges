class Cipher {
	constructor(key) {
		if (key === undefined) {
			this.key = 'dddddddddd';
		} else if (!this.isValidKey(key)) {
			throw Error('Bad key');
		} else {
			this.key = key;
		}
		this.alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
	}

	encode(plainText) {
		plainText = plainText.split('');
		let cipherText = '';

		for (let index = 0; index < plainText.length; index++) {
			let plainTextLetter = plainText[index];
			let keyLetter = this.key[index % this.key.length];

			let newIndex = this.alphabet.indexOf(plainTextLetter) + this.alphabet.indexOf(keyLetter);
			newIndex %= this.alphabet.length;

			cipherText += this.alphabet[newIndex];
		}
		return cipherText;
	}

	decode(cipherText) {
		cipherText = cipherText.split('');
		let plainText = '';

		for (let index = 0; index < cipherText.length; index++) {
			let cipherTextLetter = cipherText[index];
			let keyLetter = this.key[index % this.key.length];

			let newIndex = this.alphabet.indexOf(cipherTextLetter) - this.alphabet.indexOf(keyLetter);
			newIndex += this.alphabet.length;
			newIndex %= this.alphabet.length;

			plainText += this.alphabet[newIndex];
		}
		return plainText;
	}

	isValidKey(key) {
		return key !== '' && !this.keyHasNumber(key) && !this.keyIsUpperCase(key);
	}

	keyHasNumber(key) {
		return /\d/.test(key);
	}

	keyIsUpperCase(key) {
		return key === key.toUpperCase();
	}
}

module.exports = Cipher;