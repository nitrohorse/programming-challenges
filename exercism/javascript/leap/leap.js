//
// This is only a SKELETON file for the "Leap" exercise. It's been provided as a
// convenience to get you started writing code faster.
//

var Year = function (input) {
	this.year = input;
};

Year.prototype.isLeap = function () {
	// if (year is not divisible by 4) then(it is a common year)
	// else if (year is not divisible by 100) then(it is a leap year)
	// else if (year is not divisible by 400) then(it is a common year)
	// else (it is a leap year)
	if (this.year % 4 !== 0) {
		return false;
	} else if (this.year % 100 !== 0) {
		return true;
	} else if (this.year % 400 !== 0) {
		return false;
	} else {
		return true;
	}
};

module.exports = Year;
