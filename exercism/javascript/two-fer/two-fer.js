var TwoFer = function () {};

TwoFer.prototype.twoFer = function (who) {
  if (who === undefined) {
    return 'One for you, one for me.';
  } else if (who === 'Alice') {
    return 'One for Alice, one for me.';
  } else if (who === 'Bob') {
    return 'One for Bob, one for me.';
  }
};

module.exports = TwoFer;
