class Clock {
  constructor (hour, minute, pm) {
    this.hour = (hour % 12)
    this.minute = (minute % 60)
    this.pm = pm

    this.angleOfMinuteHand = 6 * this.minute
    this.angleOfHourHand = 0.5 * (this.hour * 60 + this.minute)

    this.angleBetweenHourAndMinuteHands
      = Math.abs(this.angleOfHourHand - this.angleOfMinuteHand)
    this.angleBetweenHourAndMinuteHands
       = Math.min(360 - this.angleBetweenHourAndMinuteHands, this.angleBetweenHourAndMinuteHands)
  }

  getRevolutionsBetween (clock) {
    // find difference between two hour hands
    const differingAngle = Math.abs(this.angleOfHourHand - clock.angleOfHourHand)
    console.log(differingAngle)
  }

}

const clock1 = new Clock(3, 27, 'pm')
console.log('Angle of hour hand:', clock1.angleOfHourHand)
console.log('Angle of minute hand:', clock1.angleOfMinuteHand)
console.log('Angle between hour and minute hands:', clock1.angleBetweenHourAndMinuteHands)

const clock2 = new Clock(9, 00, 'pm')

console.log('Revolutions between 3:27pm and 9:00pm:', clock1.getRevolutionsBetween(clock2))