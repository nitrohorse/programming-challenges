/*
Input: dictionary[] = {"GEEKS", "FOR", "QUIZ", "GO"};
       boggle[][]   = {{'G','I','Z'},
                       {'U','E','K'},
                       {'Q','S','E'}};
      isWord(str): returns true if str is present in dictionary
                   else false.

Output:  Following words of dictionary are present
         GEEKS
         QUIZ
*/

const log = console.log

const isWord = (string, dictionary) => {
  for (let i = 0; i < dictionary.length; i++) {
    const word = dictionary[i]

    if (string === word) {
      return true
    }
  }
  return false
}

const findWords = (boggleBoard, visited, row, column, string, dictionary) => {
  visited[row][column] = true

  string += boggleBoard[row][column]

  if (isWord(string, dictionary)) {
    log(string)
  }

  const width = boggleBoard.length
  const height = boggleBoard[0].length

  for (let i = row - 1; i <= row + 1 && i < height; i++) {
    for (let j = column - 1; j <= column + 1 && j < width; j++) {
      if (i >= 0 && j >= 0 && !visited[i][j]) {
        findWords(boggleBoard, visited, i, j, string, dictionary)
      }
    }
  }

  string = string.slice(0, -1)
  visited[row][column] = false
}

const boggleSolver = (boggleBoard, dictionary) => {
  const width = boggleBoard[0].length
  const height = boggleBoard.length

  const visited = new Array(height)
  for (let i = 0; i < height; i++) {
    visited[i] = new Array(width).fill(false)
  }

  // initialize current string
  let string = ''

  for (let i = 0; i < height; i++) {
    for (let j = 0; j < width; j++) {
      findWords(boggleBoard, visited, i, j, string, dictionary)
    }
  }
}

const boggleBoard = [
  ['G', 'I', 'Z'],
  ['U', 'E', 'K'],
  ['Q', 'S', 'E']
]

const dictionary = ['GEEKS', 'FOR', 'QUIZ', 'GO']

log(boggleSolver(boggleBoard, dictionary))